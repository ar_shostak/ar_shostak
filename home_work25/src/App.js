import React from 'react';
import './App.css';
import Bar from './components/bar/Bar';
import Items from './components/items/Items';
import Header from './components/header/Header';
import { Route } from 'react-router-dom';
import Info from './components/item_info/Info';
import CreateItemContainer from './components/create/CreateItemContainer';

function App(props) {
  return (
      <div className="app_wrapper">
        <Header />
        <Bar dispatch={props.dispatch} createAutosFields={props.state.createAutosFields}/>
        <Route path='/create' render={ () => <CreateItemContainer store={props.store} /> }/>
        <Route path='/items' render={ () => <Items autosData={props.state.autosData} store={props.store} /> }/>
        <Route path='/info' render={ () => <Info autosData={props.state.autosData}/> }/>
      </div>
  );
}
export default App;