import React from 'react';
import './CreateItem.css';

const CreateItem = (props) => {
    let newItemName = React.createRef();
    let newItemPrice = React.createRef();
    let newItemImgUrl = React.createRef();

    let addItem = () => {
        props.addItemActionCreator();  
    }
       
    let onNameChange = () => {
        let text = newItemName.current.value;
        props.createItemNameActionCreator(text);
    }

    let onPriceChange = () => {
        let text = newItemPrice.current.value;
        props.createItemPriceActionCreator(text);
    }

    let onImgUrlChange = () => {
        let text = newItemImgUrl.current.value;
        props.createItemImgUrlActionCreator(text);
    }
    return (
        <div className='createItem'>
            <div>Fill out form to create a new item</div> 
            <div>
                <textarea className='field' ref={newItemName} onChange={onNameChange} value={props.createAutosFields.newItemName} placeholder='Enter name'/>
            </div>
            <div>
                <textarea className='field' ref={newItemPrice} onChange={onPriceChange} value={props.createAutosFields.newItemPrice} placeholder='Enter price'/>
            </div>
            <div>
                <textarea className='field' ref={newItemImgUrl} onChange={onImgUrlChange} value={props.createAutosFields.newItemImgUrl} placeholder='Enter url for picture'/>
            </div>
            <div ><button className='button' onClick={addItem}>Add</button></div>
        </div>
    )
}
export default CreateItem;