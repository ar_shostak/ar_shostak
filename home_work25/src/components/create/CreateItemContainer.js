import React from 'react';
import './CreateItem.css';
import {addItemActionCreator, createItemNameActionCreator, createItemPriceActionCreator, createItemImgUrlActionCreator} from '../../data/createItemReducer' 
import CreateItem from './CreateItem';


const CreateItemContainer = (props) => {
    let newItemName = React.createRef();
    let newItemPrice = React.createRef();
    let newItemImgUrl = React.createRef();

    let addItem = () => { 
        props.store.dispatch(addItemActionCreator());  
    }
       
    let onNameChange = (text) => {
        let action = createItemNameActionCreator(text);
        props.store.dispatch(action);
    }

    let onPriceChange = (text) => {
        let action = createItemPriceActionCreator(text);
        props.store.dispatch(action);
    }

    let onImgUrlChange = (text) => {
        let action =  createItemImgUrlActionCreator(text);
        props.store.dispatch(action);
    }
    return (<CreateItem createItemImgUrlActionCreator = {onImgUrlChange}
        createItemPriceActionCreator = {onPriceChange}
        createItemNameActionCreator = {onNameChange}
        addItemActionCreator = {addItem}
        createAutosFields = {props.store.getState().data.createAutosFields}/>) 
}
export default CreateItemContainer;