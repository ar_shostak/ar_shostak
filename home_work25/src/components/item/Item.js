import React from 'react';
import './Item.css';
import { NavLink } from 'react-router-dom';

const Item = (props) => {
    return (
        <div className='item'>
            <div>{props.name} {props.price} $</div>
            <NavLink to = '/info'><div><img src={props.url} /></div></NavLink>
    </div>    
    )
}
export default Item;