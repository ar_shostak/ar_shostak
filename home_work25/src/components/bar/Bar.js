import React from 'react';
import './Bar.css';
import { NavLink } from 'react-router-dom';

const Bar = (props) => {

    return (
        <nav className='bar'>
            <div className='operation'>
                <div className='text'><NavLink to='/create'>Create</NavLink></div>
                <div className='text'><NavLink to='/delete'>Delete</NavLink></div>
                <div className='text'><NavLink to='/items'>Items</NavLink></div>
            </div>
        </nav>
    )
}
export default Bar;