import React from 'react';
import './Items.css';
import Item from './../item/Item.js';

const Items = (props) => {
       let autosItem =props.autosData.map(el => (<li><Item name={el.name} price={el.price} url={el.url} /></li>));
    return (
        <div className='items'>
            <ul>
                {autosItem}    
            </ul>
        </div>       
    )}
export default Items;