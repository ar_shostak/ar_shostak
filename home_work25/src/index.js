import * as serviceWorker from './serviceWorker';
//import store from './data/store'
import store from './data/reduxStore';
import React from 'react';
import './index.css';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

let renderEntireTree = (state) => {
    ReactDOM.render(
        <BrowserRouter>
           <App state={state.data} dispatch={store.dispatch.bind(store)} store={store} />
        </BrowserRouter>, document.getElementById('root'));
}

renderEntireTree(store.getState())

store.subscribe(() => {
    renderEntireTree(store.getState());
});

serviceWorker.unregister();