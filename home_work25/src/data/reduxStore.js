import { createStore, combineReducers } from "redux";
import createItemReducer from './createItemReducer';

let reducers = combineReducers({
    data: createItemReducer
});

let store = createStore(reducers);

export default store; 