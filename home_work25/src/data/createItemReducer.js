const ADD_ITEM = 'ADD_ITEM';
const CREATE_ITEM_NAME = 'CREATE_ITEM_NAME';
const CREATE_ITEM_PRICE = 'CREATE_ITEM_PRICE';
const CREATE_ITEM_IMG_URL = 'CREATE_ITEM_IMG_URL';

let initialState = {
    createAutosFields: [ 
        {newItemName: ''},
        {newItemPrice: ''},
        {newItemImgUrl: ''},
    ],
    autosData: [
        {name: 'Audi RS3', price: '45.6', url: 'https://img.tyt.by/720x720s/n/avto/02/3/abt_audi_rs3_1.jpg'},  
        {name: 'VW Golf R', price: '41.2', url: 'https://a.d-cd.net/127877as-960.jpg'},
        {name: 'Mercedes A45 AMG', price: '49.8', url: 'https://autoreview.ru/images/gallery/%D0%9D%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D0%B8/2019/July/04/mercedes-amg-a45-3.jpg'},
        {name: 'BMW M1', price: '48.4', url: 'https://a.d-cd.net/2719766s-960.jpg'},
        {name: 'Toyota supra', price: '40.5', url: 'https://s.auto.drom.ru/i24229/c/photos/fullsize/toyota/supra/toyota_supra_871424.jpg'}
    ],  
};    

const createItemReducer = (state = initialState, action) => {
    switch(action.type) {
    case ADD_ITEM:
        return _addItem(state);;
    case CREATE_ITEM_NAME:
        return _createItemName(action.newText, state);
    case CREATE_ITEM_PRICE:
        return _createItemPrice(action.newText, state);
    case CREATE_ITEM_IMG_URL:
        return _createItemImgUrl(action.newText, state);
    default:
        return state; 
    }
}

let _addItem = (state) => {
        let newItem = {
            name: state.createAutosFields.newItemName,
            price: state.createAutosFields.newItemPrice,
            url: state.createAutosFields.newItemImgUrl,
        };
        state.autosData.push(newItem);
        _createItemName('', state);
        _createItemPrice('', state);
        _createItemImgUrl('', state);
        return state;
   }

   let _createItemName = (newText, state) => {
        state.createAutosFields.newItemName = newText;
        return state;
    }

   let _createItemPrice = (newText, state) => {
      state.createAutosFields.newItemPrice = newText;
    return state;
    }

   let _createItemImgUrl = (newText, state) => {
       state.createAutosFields.newItemImgUrl = newText;
       return state;
    }
 
export const addItemActionCreator = () => ({ type: ADD_ITEM } )
 
export const createItemNameActionCreator = (text) => ({
    type: CREATE_ITEM_NAME, newText: text})

export const createItemPriceActionCreator = (text) => ({
    type: CREATE_ITEM_PRICE, newText: text})

export const createItemImgUrlActionCreator = (text) => ({
    type: CREATE_ITEM_IMG_URL, newText: text})

 export default createItemReducer;