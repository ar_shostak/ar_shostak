package mog.epam.java_course.home_work05;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class UserInterfaceTest {
    private String path1;
    private String path2;
    private Component actual1;
    private Component actual2;

    @Before
    public void init() {
        path1 = "root/folder1/java.txt";
        path2 = "root/folder1/java22.txt";
        actual1 = new ComponentBuilder().build(path1.split("/"));
        actual2 = new ComponentBuilder().build(path2.split("/"));
    }

    @After
    public void clear() {
        path1 = null;
        path2 = null;
        actual1 = null;
        actual2 = null;
    }

    @Test
    public void testMainMerge() throws MergerException {
        Component expected = new Folder("root", new ArrayList<Component>());
        Component innerFolder = new Folder("folder1", new ArrayList<Component>());
        innerFolder.add(new File("java", "txt"));
        innerFolder.add(new File("java22", "txt"));
        expected.add(innerFolder);
        Component actual = Merger.merge(actual1, actual2);
        assertEquals(expected, actual);
    }

    @Test
    public void testMainRootElement() {
        Component expected = new Folder("root", new ArrayList<Component>());
        Component innerFolder = new Folder("folder1", new ArrayList<Component>());
        innerFolder.add(new File("java", "txt"));
        expected.add(innerFolder);
        assertEquals(expected, actual1);
    }

    @Test
    public void testValidate() {
        boolean expected = true;
        boolean actual = Parser.validate(path1);
        assertEquals(expected, actual);
    }

    @Test
    public void testValidateNegative() {
        boolean expected = false;
        boolean actual = Parser.validate("root/folder1/rt.er.er.");
        assertEquals(expected, actual);
    }
}
