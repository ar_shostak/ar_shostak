package mog.epam.java_course.home_work05;

import java.util.regex.Pattern;

/**
 * Class for checking whether user data matches the accepted template.
 */
public class Parser {

    /**
     * Method for checking whether user data matches the accepted template
     * @param str user's data
     * @return logical expression value if data correct
     */
    public static boolean validate(String str) {
        String[] elements = str.split("/");

        boolean isCorrect = true;
        Pattern componentPattern = Pattern.compile("\\w+\\.?\\w+");
        for (String current : elements) {
            if (!current.matches(componentPattern.toString())) {
                isCorrect = false;
                break;
            }
        }
        return isCorrect;
    }
}
