package mog.epam.java_course.home_work05;

public class MergerException extends Exception {
    MergerException() {
        System.out.println("Folder or file with this name already exist");
    }
}
