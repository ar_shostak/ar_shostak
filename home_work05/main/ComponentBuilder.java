package mog.epam.java_course.home_work05;

import java.util.ArrayList;

/**
 * Calls the method for Component creation based on user's path.
 */
public class ComponentBuilder {

    /**
     * Calls the method for Component creation based on user's path.
     * @param elements an array containing the path elements entered by the user
     */
    public Component build(String[] elements) {
        Component component;
        String name = elements[0];
            if (isFolder(elements[0]) == true) {
                if (elements.length == 1) {
                    component = buildFolder(elements[0]);
                } else {
                    String[] newElements = new String[elements.length - 1];
                    for (int i = 0; i < elements.length - 1; i++) {
                        newElements[i] = elements[i + 1];
                    }
                    component = buildFolder(name, newElements);
                }
            } else {
                component = buildFile(elements);
            }
        return component;
    }

    /**
     * Creates Folder. This overloading method contains the recursion calling of the build method
     * @param name the line with parameters for Folder creation
     * @param newElements an array containing the path elements entered by the user
     * @return created Folder
     */
    private Component buildFolder(String name, String[] newElements) {
        Folder folder = new Folder(name, new ArrayList<Component>());
        folder.add(build(newElements));
        return folder;
    }

    /**
     * Creates Folder. This overloading method will be calls the latest because it has no recursion
     * @param name the line with parameters for Folder creation
     * @return created Folder
     */
    private Component buildFolder(String name) {
        Folder folder = new Folder(name, new ArrayList<Component>());
        return folder;
    }

    /**
     * Creates File
     * @param str the array contains the line with parameters for File creation
     * @return created file
     */
    private Component buildFile(String[] str) {
        String[] args = str[0].split("\\.");
        File file = new File(args[0], args[1]);
        return file;
    }

    /**
     * Checks if a transferred argument corresponds to the syntax of a file name
     * @param name string to be checked
     * @return the result of the boolean expression
     */
    private boolean isFolder(String name) {
        if (name.contains(".")) {
            return false;
        }
        return true;
    }
}
