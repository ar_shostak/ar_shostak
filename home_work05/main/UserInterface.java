package mog.epam.java_course.home_work05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Reads from the keyboard the path to the file or directory. If the path is repeated
 * throws an Exception and waiting for a re-entry. For output on screen you need
 * to enter the keyword "print", to exit - "exit". The first path is the root, others
 * merge with him. The path must start with "root" Folder
 */
public class UserInterface {

    public static void main(String[] args) throws IOException, MergerException {
        Boolean isFather = true;
        Component component = new Folder("root", new ArrayList<Component>());
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while(true) {
                String str = reader.readLine();
                if (str.equals("exit")) {
                    break;
                } else if(str.equals("print")) {
                    component.print(0);
                    continue;
                }
                try {
                    if ((Parser.validate(str) == true) && (isFather == true)) {
                        component = new ComponentBuilder().build(str.split("/"));
                        isFather = false;
                    } else if ((Parser.validate(str) == true) && (isFather == false)) {
                        Component compFather = component;
                        Component compChild = new ComponentBuilder().build(str.split("/"));
                        component = Merger.merge(compFather, compChild);
                    } else {
                        throw new IOException("InvalidInput");
                    }
                } catch (MergerException e) {
                       System.out.println("Try again");
                }
            }
        }
    }

}
