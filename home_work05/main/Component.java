package mog.epam.java_course.home_work05;

/**
 * Represents an interface for working with the file system
 */
public interface Component {
    /**
     * Adds component for the current component. In file it has an empty
     * implementation. Correctness is provided by the user interface
     * @param component component for adding
     */
    public void add(Component component);

    /**
     * Displays this object in the form for submission to the directory
     * @param i the number of points on which shifted representation of this object in the directory
     */
    public void print(int i);

    /**
     * Return the name of current component
     */
    public String getName();
}
