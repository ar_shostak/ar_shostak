package mog.epam.java_course.home_work02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class Task01Test {
    Task01 object;
    @Before
    public void initTask01() {
        object = new Task01();
    }

    @After
    public void clearTask01() {
        object = null;
    }

    @Test
    public void testMain() {
        Task01.main(new String[] {"2", "3", "3.5", "5.6"});
        double actual = Task01.g;
        double expected = (4 * Math.pow(Math.PI, 2)) * Math.pow(2, 3) / (3 * 3 * (3.5 + 5.6));
        org.junit.Assert.assertEquals(expected, actual, 0.01);
    }

    @Test
    public void testMainWrongData() {
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        Task01.main(new String[] {"2", "3", "3.5"});
        org.junit.Assert.assertEquals("Invalid input", str.toString());
    }

    @Test
    public void testMainDevisionByZero() {
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        Task01.main(new String[] {"2", "0", "3.5", "5"});
        org.junit.Assert.assertEquals("Invalid input", str.toString());
    }
}
