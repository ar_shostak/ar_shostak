package mog.epam.java_course.home_work02;

import org.junit.Test;

public class Task02Test {
    Task02 object = new Task02();

    @Test
    public void testFindFibonacciWithWhileCycle() {
        int[] actual = object.findFibonacci(1, 7);
        int[] expected = new int[] {0, 1, 1, 2, 3, 5, 8};
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindFibonacciWithDoWhileCycle() {
        int[] actual = object.findFibonacci(2, 7);
        int[] expected = new int[] {0, 1, 1, 2, 3, 5, 8};
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindFibonacciWithForCycle() {
        int[] actual = object.findFibonacci(3, 7);
        int[] expected = new int[] {0, 1, 1, 2, 3, 5, 8};
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindFactorialWithWhileCycle() {
        int[] actual = object.findFactorial(1, 7);
        int[] expected = new int[] { 5040 };
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindFactorialWithDoWhileCycle() {
        int[] actual = object.findFactorial(2, 7);
        int[] expected = new int[] { 5040 };
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindFactorialWithForCycle() {
        int[] actual = object.findFactorial(3, 7);
        int[] expected = new int[] { 5040 };
        org.junit.Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testIsSourceDataCorrectNegativeAlgorithmId() {
        Boolean actual = Task02.isSourceDataCorrect("3", 2, 7);
        Boolean expected = false;
        org.junit.Assert.assertTrue(expected == actual);
    }

    @Test
    public void testIsSourceDataCorrectNegativeN() {
        Boolean actual = Task02.isSourceDataCorrect("1", 2, -1);
        Boolean expected = false;
        org.junit.Assert.assertTrue(expected == actual);
    }
}
