package mog.epam.java_course.home_work03;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;
public class CardTest {
    Card card;
    Card card2;
    @Before
    public void initTask01() {
        card = new Card("Artyom");
        card2 = new Card("Artyom", new BigDecimal(35.55));
    }

    @After
    public void clearTask01() {
        card = null;
        card2 = null;
    }

    @Test(expected = NumberFormatException.class)
    public void testCardConstrWithOneWrongArg() {
        Card actual = new Card("123");
        String expected = "Exception expected";
        assertEquals(expected, actual.getHolderName());
    }

    @Test
    public void testCardConstrWithOneArg() {
        Card actual = new Card("Paul");
        String expected = "Paul";
        assertEquals(expected, actual.getHolderName());
    }

    @Test(expected = NullPointerException.class)
    public void testCardConstrWithNull() throws NullPointerException {
        Card actual = new Card(null);
        Card expected = card;
        assertEquals("We expected exception here", expected, actual);
    }

    @Test
    public void testCardConstrWithTwoArg() {
        Card actual = new Card("Artyom", new BigDecimal(35.55));
        assertEquals(card2, actual);
    }
    @Test
    public void testGetHolderName() {
        String actual = card2.getHolderName();
        String expected = "Artyom";
        assertEquals(expected, actual);
    }
    @Test
    public void testGetBalance() {
        BigDecimal actual = card2.getBalance();
        BigDecimal expected = new BigDecimal(35.55);
        assertEquals(expected, actual);
    }


    @Test
    public void testSetHolderName() {
        card.setHolderName("Boris");
        String expected = "Boris";
        assertEquals(card.getHolderName(), expected);
    }

    @Test
    public void testSetBalance() {
        card.setBalance(new BigDecimal(100));
        BigDecimal expected = new BigDecimal(100);
        assertEquals(card.getBalance(), expected);
    }

    @Test
    public void testAddTo() {
        BigDecimal expected = new BigDecimal(100);
        card.addTo(new BigDecimal(100));
        BigDecimal actual = card.getBalance();
        assertEquals(expected,actual);
    }

    @Test
    public void testWithdraw() {
        BigDecimal expected = new BigDecimal(35);
        BigDecimal actual = card2.withdraw(new BigDecimal(35));
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawWithTooBigAmount() {
        BigDecimal expected = null;
        BigDecimal actual = card2.withdraw(new BigDecimal(40.55));
        assertEquals(expected, actual);
    }

    @Test
    public void testDispalyBalanceInCurrency() {
        OutputStream str = new ByteArrayOutputStream();
        System.setOut(new PrintStream(str));
        Double rate = 0.5;
        String expected = "Balance = 12.50000";
        card.setBalance(new BigDecimal(25));
        card.dispalyBalanceInCurrency(rate);
        assertEquals(expected, str.toString());
    }

    /*

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Card card = (Card) o;
        if (balance == null) {
            if (card.balance != null) {
                return false;
            } else if (!balance.equals(card.balance)){
                 return false;
            }
        } if (!holderName.equals(card.holderName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode())
                + ((holderName == null) ? 0 : holderName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "holderName='" + holderName + '\'' +
                ", balance=" + balance +
                '}';
    }
     */
}
