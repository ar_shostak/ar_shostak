package mog.epam.java_course.home_work03;

import java.util.Arrays;

public final class Median {

    private Median() {}

    /**
     * Method median find middle value in a sorted list of numbers. Acceps
     * {@code int} data
     * @param args an array contains the source data of {@code int} type
     * @return the value of median in type of {@code float}
     */
    public static float median(int[] args) {
        float result = 0;
        Arrays.sort(args);

        if (args.length % 2 == 0) {
            result = (float) (0.5 * (args[(args.length / 2) - 1] + args[args.length / 2]));

        } else if (args.length % 2 == 1) {
            result = (float) args[(args.length - 1) / 2];
        }
        return result;
    }

    /**
     * Method median find middle value in a sorted list of numbers. Acceps
     * {@code double} data
     * @param args an array contains the source data of {@code double} type
     * @return the value of median in type of {@code double}
     */
    public static double median(double[] args) {
        double result = 0;
        Arrays.sort(args);

        if (args.length % 2 == 0) {
            result = 0.5 * (args[(args.length / 2) - 1] + args[args.length / 2]);

        } else if (args.length % 2 == 1) {
            result = args[(args.length - 1) / 2];
        }
        return result;
    }
}
