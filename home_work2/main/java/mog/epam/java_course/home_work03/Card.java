package mog.epam.java_course.home_work03;

import java.math.BigDecimal;

/**
 * The {@code Card} class provides operations for add to,
 * withdraw and viewing balance. Balance stored in {@code BigDecimal}
 */
public class Card {
    private String holderName;
    private BigDecimal balance;

    /**
     * Public constructor that create a new card with only owner's name argument.
     * Defult balance value is set to 0;
     * @param holderName card owner's name consists of letters only!
     */
    public Card(String holderName) {
        if (!holderName.matches("[A-Za-z]+")) {
            throw new NumberFormatException("Bad input.");
        }
        this.holderName = holderName;
        this.balance = new BigDecimal(0);
    }

    /**
     * Public constructor that create a new card with name and balance recieved as arguments.
     * Defult balance value is set to 0;
     * @param holderName card owner's name consists of letters only!
     * @param balance grater than 0;
     */
    public Card(String holderName, BigDecimal balance) {
        if (!(holderName.matches("[A-Za-z]+")) || (balance.compareTo(BigDecimal.ZERO) < 0)) {
            throw new NumberFormatException("Bad input.");
        }
        this.holderName = holderName;
        this.balance = balance;
    }

    /**
     * @return the value of holderName field
     */
    public String getHolderName() {
        return holderName;
    }

    /**
     * @return the value of balance field
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * Assign the value for holderName field
     *@param holderName this value will be assigned as a class field value
     */
    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    /**
     * Assign the value for balance field
     *@param balance this value will be assigned as a class field value
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * Add to the ballance the value of argument
     * @param amount value will be added to the current balance
     */
    public void addTo(BigDecimal amount) {
        balance = balance.add(amount);
    }

    /**
     * Substract from the balance the value of the amount. If amount is greate
     * than balance, return null else return amount
     * @param amount value will be substract from the current balance.
     * @return can be null or the amount value
     */
    public BigDecimal withdraw(BigDecimal amount) {
        if (amount.compareTo(balance) <= 0) {
            balance = balance.subtract(amount);
            return amount;
        } else {
            return null;
        }
    }

    /**
     * Display the balance value in given exchange rate If amount is greate
     *  @param rate exchange rate
     */
    public void dispalyBalanceInCurrency(double rate) {
        BigDecimal result = exchange(rate);
        System.out.print("Balance = " + result);
    }

    /**
     * Translates the balance in other currency by providing rate
     *  @param rate exchange rate
     * @return balance in other currency
     */
    private BigDecimal exchange(double rate) {
        BigDecimal exchanged = balance.multiply(new BigDecimal(rate));
        return exchanged.setScale(5);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Card card = (Card) o;
        if (balance == null) {
            if (card.balance != null) {
                return false;
            } else if (!balance.equals(card.balance)){
                 return false;
            }
        } if (!holderName.equals(card.holderName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode())
                + ((holderName == null) ? 0 : holderName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Card{" +
                "holderName='" + holderName + '\'' +
                ", balance=" + balance +
                '}';
    }
}