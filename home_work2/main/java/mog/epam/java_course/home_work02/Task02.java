package mog.epam.java_course.home_work02;
public class Task02 {

    /**
     * Calculate the value according to user's algorithm and  loop type
     * @param args {@code Array} that contains user's data
     */
    public static void main(String[] args) {
        final String ALGORITHM_ID_INDEX = args[0];
        final int LOOP_TYPE_ID_INDEX = Integer.parseInt(args[1]);
        final int NUMBER = Integer.parseInt(args[2]);

        int[] result = new int[0];
        if (isSourceDataCorrect(ALGORITHM_ID_INDEX, LOOP_TYPE_ID_INDEX, NUMBER)) {
            if (ALGORITHM_ID_INDEX.equals("1")) {
                result = findFibonacci(LOOP_TYPE_ID_INDEX, NUMBER);

                System.out.printf("First %d numbers of the Fibonacci serial is ", NUMBER);
                for (int i : result) {
                    System.out.print(i + " ");
                }
            }  else if (ALGORITHM_ID_INDEX.equals("2")) {
                result = findFactorial(LOOP_TYPE_ID_INDEX, NUMBER);

                System.out.printf("Factorial value of %d is ", NUMBER);
                for (int i : result) {
                    System.out.print(i);
                }
            }
        } else {
            System.out.println("Invalid input");
        }
    }

    /**
     * Check user's data.
     * @param algorithmId {@code String} value that determines what
     * calculation need to be done.
     * @param loopType {@code int} value that determinate what type of loop
     * will be used
     * @param n number for calculations. If n less then 0, return false
     * @return boolean expression true if user's data valid
     */
    static boolean isSourceDataCorrect(String algorithmId, int loopType, int n) {
        if (!(algorithmId.equals("1") || algorithmId.equals("2"))) {
            return false;
        } else if (loopType < 1 || loopType > 3) {
            return false;
        } else if (n < 0) {
            return false;
        }
        return true;
    }

    /**
     * Returns the {@code array} containing the factorial value of given number
     * @param loopType {@code int} value that determinate what type of loop
     * will be used
     * @param n number for factorial to be fond of
     * @return the factorial value
     */
    public static int[] findFactorial(int loopType, int n) {
        int result = 1;
        int counter;
        if (n == 0){
            return new int[] { 1 };
        }
        switch (loopType) {
            case 1:
                counter = 0;
                while (counter < n) {
                    result = result * (counter + 1);
                    counter++;
                }
                break;
            case 2:
                counter = 0;
                do {
                    result = result * (counter + 1);
                    counter++;
                } while (counter < n);
                break;
            case 3:
                for (int i = 1; i <= n; i++) {
                    result = result * i;
                }
                break;
        }
        return new int[] {result};
    }

    /**
     * Returns the {@code array} containing the specified number of fibonacci numbers
     * @param loopType {@code int} value that determinate what type of loop
     * will be used in calculation
     * @param n number of returned Fibonacci numbers
     * @return the factorial value
     */
    public static int[] findFibonacci(int loopType, int n) {
        int[] result = new int[n];
        int currentElement = 0;
        int previousElement = 0;
        int index = 0;
        int buffer = 1;

        switch (loopType) {
            case 1:
                while (index < n) {
                    result[index] = currentElement;
                    buffer += previousElement;
                    previousElement = currentElement;
                    currentElement = buffer;
                    index++;
                }
                break;
            case 2:
                do {
                    result[index] = currentElement;
                    buffer += previousElement;
                    previousElement = currentElement;
                    currentElement = buffer;
                    index++;
                } while (index < n);
                break;
            case 3:
                for (; index < n; index++) {
                    result[index] = currentElement;
                    buffer += previousElement;
                    previousElement = currentElement;
                    currentElement = buffer;
                }
                break;
            }
        return result;
    }
}
