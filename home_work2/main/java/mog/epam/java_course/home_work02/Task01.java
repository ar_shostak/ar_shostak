package mog.epam.java_course.home_work02;

 class Task01 {
     static double g;

    /**
     *Display the value of the reduced function if null deviding don't occure.
     * @param args the array contains {@code String} representation of users arguments
     */
    public static void main(String[] args) {
        int a;
        int p;
        double m1;
        double m2;

        if (args.length == 4) {
            a = Integer.parseInt(args[0]);
            p = Integer.parseInt(args[1]);
            m1 = Double.parseDouble(args[2]);
            m2 = Double.parseDouble(args[3]);
            if ((m1 + m2) != 0 && (p != 0)) {
                g = calculate(a, p, m1, m2);
                System.out.println("G = " + g);
            } else {
                System.out.print("Invalid input");
            }
        } else {
            System.out.print("Invalid input");
        }
    }

    /**
     *Calculate the value of the reduced function.
     * @param a the first user's argument, has {@code int} type 
     * @param p second user's argument, has {@code int} type, not null 
     * @param m2 third user's argument, has {@code double} type 
     * @param m1 fourth user's argument, has {@code double} type 
     */
    private static double calculate(int a, int p, double m1, double m2) {
        double firstMultiplyer = 4 * Math.pow(Math.PI, 2);
        double secondMultiplyer = Math.pow(a, 3) / (p * p * (m1 + m2));
        return firstMultiplyer * secondMultiplyer;
    }
}
