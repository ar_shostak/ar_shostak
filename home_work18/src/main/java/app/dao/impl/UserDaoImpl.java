package app.dao.impl;

import app.connection.ConnectionProvider;
import app.dao.UserDao;
import app.entity.User;
import app.service.ContextUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserDaoImpl implements UserDao {
    private static final String ID_COLUMN = "id";
    private static final String USER_NAME_COLUMN = "userName";
    private static final String PASSWORD_COLUMN = "password";
    private static final String CONNECTION_PROVIDER = "connectionProvider";
    private static final String SELECT_USER = "SELECT id, userName, password FROM Users WHERE userName LIKE ?";
    private static final String SELECT_ALL_USERS = "SELECT * FROM Users";
    private static final String INSERT_USER = "INSERT INTO Users (userName, password) VALUES (?,?)";
    private static final int USER_NAME = 1;
    private static final int PASSWORD = 2;

    private ConnectionProvider provider;

    public UserDaoImpl() {

        this.provider = (ConnectionProvider) ContextUtil.getContext().getBean(CONNECTION_PROVIDER);
    }

    /**
     * Returns the user by the transferred name
     * @param userName user's name
     * @return the {@link User}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public Optional<User> getUserByName(String userName) throws SQLException {
        Optional<User> user = Optional.empty();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, userName);
             ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                user = Optional.of(createUser(rs));
            }
        }
        return user;
    }

    /**
     * Return list contains all users in base
     * @return the list with all users
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public List<User> getAllUsers() throws SQLException {
        List<User> userList = new ArrayList<>();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = conn.prepareStatement(SELECT_ALL_USERS);
             ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                userList.add(createUser(rs));
            }
        }
        return userList;
    }

    /**
     * Add user in base by the transferred name
     * @param userName user's name
     * @param password user's password
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public void addUser(String userName, String password) throws SQLException {
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, Stream.of(new Object[][]{
                     {USER_NAME_COLUMN, userName},
                     {PASSWORD_COLUMN, password},
             }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])))) {
            st.executeUpdate();
        }
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con    the Connection
     * @param param param for SQL statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Object param) throws SQLException {
        PreparedStatement ps = con.prepareStatement(UserDaoImpl.SELECT_USER);
        ps.setObject(USER_NAME, param);
        return ps;
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con    the Connection
     * @param params params for SQL statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Map<String, Object> params) throws SQLException {
        PreparedStatement ps = con.prepareStatement(UserDaoImpl.INSERT_USER);
        ps.setObject(USER_NAME, params.get(USER_NAME_COLUMN));
        ps.setObject(PASSWORD, PASSWORD_COLUMN);
        return ps;
    }

    private User createUser(ResultSet rs) throws SQLException {
        return new User(rs.getLong(ID_COLUMN), rs.getString(USER_NAME_COLUMN), rs.getString(PASSWORD_COLUMN));
    }
}

