package app.dao.impl;

import app.connection.ConnectionProvider;
import app.dao.OrderGoodsDao;
import app.entity.OrderGoods;
import app.service.ContextUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderGoodsDaoImpl implements OrderGoodsDao {
    private static final String ID_COLUMN = "id";
    private static final String ORDER_ID_COLUMN = "orderId";
    private static final String GOOD_ID_COLUMN = "goodId";
    private static final String CONNECTION_PROVIDER = "connectionProvider";
    private static final String SELECT_ORDER_GOOD_BY_ORDER_ID = "SELECT * FROM OrderGoods WHERE orderId LIKE ?";
    private static final String INSERT_ORDER_GOOD= "INSERT INTO OrderGoods (orderId, goodId) VALUES (?,?)";
    private static final int ORDER_ID = 1;
    private static final int GOOD_ID = 2;

    private ConnectionProvider provider;

    public OrderGoodsDaoImpl(){
        this.provider = (ConnectionProvider) ContextUtil.getContext().getBean(CONNECTION_PROVIDER);
    }

    /**
     * Return list contains all {@link OrderGoods} of the transferred orderId
     * @return the list with all {@link OrderGoods} of this order
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public List<OrderGoods> getByOrderId(Long orderId) throws SQLException {
        List<OrderGoods> list = new ArrayList<>();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, orderId);
             ResultSet rs = st.executeQuery()){

            while (rs.next()) {
                list.add(new OrderGoods(rs.getLong(ID_COLUMN), rs.getLong(ORDER_ID_COLUMN), rs.getLong(GOOD_ID_COLUMN)));
            }
        }
        return list;
    }

    /**
     * Add to OrderGoods table units with transferred goodId and orderId
     * @param orderId the order id
     * @param goodId the good id
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public void addToOrderGood(Long orderId, Long goodId) throws SQLException {
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, Stream.of(new Object[][]{
                     {ORDER_ID_COLUMN, orderId},
                     {GOOD_ID_COLUMN, goodId},
             }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])))) {

            st.executeUpdate();
        }
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con    the Connection
     * @param param param for SQL statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Object param) throws SQLException {
        PreparedStatement ps = con.prepareStatement(OrderGoodsDaoImpl.SELECT_ORDER_GOOD_BY_ORDER_ID);
        ps.setObject(ORDER_ID, param);
        return ps;
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con    the Connection
     * @param params params for SQL statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Map<String, Object> params) throws SQLException {
        PreparedStatement ps = con.prepareStatement(OrderGoodsDaoImpl.INSERT_ORDER_GOOD);
        ps.setObject(ORDER_ID, params.get(ORDER_ID_COLUMN));
        ps.setObject(GOOD_ID, params.get(GOOD_ID_COLUMN));
        return ps;
    }
}
