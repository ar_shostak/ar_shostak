package app.dao.impl;

import app.connection.ConnectionProvider;
import app.dao.OrderDao;
import app.entity.Order;
import app.service.ContextUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class OrderDaoImpl implements OrderDao {
    private static final String ID_COLUMN = "id";
    private static final String USER_ID_COLUMN = "userId";
    private static final String TOTAL_PRICE_COLUMN = "totalPrice";
    private static final String CONNECTION_PROVIDER = "connectionProvider";
    private static final String SELECT_ORDER_BY_USER = "SELECT * FROM Orders WHERE userId = ?";
    private static final String INSERT_ORDER = "INSERT INTO Orders (userId) VALUES (?)";
    private static final String UPDATE_ORDER = "UPDATE Orders SET totalPrice = ? WHERE userId = ?";
    private static int ID = 1;
    private static int TOTAL_PRICE = 2;

    private ConnectionProvider provider;

    public OrderDaoImpl() {
        this.provider = (ConnectionProvider) ContextUtil.getContext().getBean(CONNECTION_PROVIDER);
    }

    /**
     * Returns the order by the transferred user's id
     * @param id user's id
     * @return the {@link Order}
     * @throws SQLException An exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public Optional<Order> getOrderByUserId(Long id) throws SQLException {
        Optional<Order> order = Optional.empty();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, SELECT_ORDER_BY_USER, id);
             ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                order = Optional.of(new Order(rs.getLong(ID_COLUMN), rs.getLong(USER_ID_COLUMN), rs.getDouble(TOTAL_PRICE_COLUMN)));
            }
        }
        return order;
    }

    /**
     * Add to Order table by the transferred user's id
     * @param userId id user's id
     * @throws SQLException An exception that provides information on a database access
     *  error or other errors.
     */
    @Override
    public void addToOrder(Long userId) throws SQLException {
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, INSERT_ORDER, userId)) {
            st.executeUpdate();
        }
    }

    /**
     * Update user's Order with transferred price
     * @param totalPrice price to be assigned to the order
     * @param userId     id of user to update
     */
    @Override
    public void updateOrderById(double totalPrice, long userId) {
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, totalPrice, userId)) {
            st.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException("Update order exception", e);
        }
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con   the Connection
     * @param sql   SQL Statement
     * @param param param for SQL statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     *  error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, String sql, Object param) throws SQLException {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setObject(ID, param);
        return ps;
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con   the Connection
     * @param totalPrice  the total price of the order
     * @param userId the User id
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     *  error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Object totalPrice, Object userId) throws SQLException {
        PreparedStatement ps = con.prepareStatement(OrderDaoImpl.UPDATE_ORDER);
        ps.setObject(ID, totalPrice);
        ps.setObject(TOTAL_PRICE, userId);
        return ps;
    }
}
