package app.dao.impl;

import app.connection.ConnectionProvider;
import app.dao.GoodDao;
import app.entity.Good;
import app.entity.User;
import app.service.ContextUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GoodDaoImpl implements GoodDao {
    private static final String ID_COLUMN = "id";
    private static final String NAME_COLUMN = "name";
    private static final String PRICE_COLUMN = "price";
    private static final String CONNECTION_PROVIDER = "connectionProvider";
    private static final String SELECT_GOOD = "SELECT * FROM Goods WHERE name LIKE ?";
    private static final String SELECT_GOOD_BY_ID = "SELECT * FROM Goods WHERE id LIKE ?";
    private static final String SELECT_ALL_GOODS = "SELECT * FROM Goods";

    private ConnectionProvider provider;

    public GoodDaoImpl() {
        this.provider = (ConnectionProvider) ContextUtil.getContext().getBean(CONNECTION_PROVIDER);
    }

    /**
     * Returns the good by the transferred name
     * @param name good's name
     * @return the {@link Good}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public Optional<Good> getGood(String name) throws SQLException {
        Optional<Good> good = Optional.empty();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, name, SELECT_GOOD);
             ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                good = Optional.ofNullable(new Good(rs.getLong(ID_COLUMN), rs.getString(NAME_COLUMN), rs.getDouble(PRICE_COLUMN)));
            }
        }
        return good;
    }

    /**
     * Returns the good by the transferred id
     * @param goodId good's id
     * @return the {@link Good}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public Optional<Good> getGood(Long goodId) throws SQLException {
        Optional<Good> good = Optional.empty();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = createPreparedStatement(conn, goodId, SELECT_GOOD_BY_ID);
             ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                good = Optional.ofNullable(new Good(rs.getLong(ID_COLUMN), rs.getString(NAME_COLUMN), rs.getDouble(PRICE_COLUMN)));
            }
        }
        return good;
    }

    /**
     * Return list contains all Goods in base
     * @return the list
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    @Override
    public List<Good> getAllGoods() throws SQLException {
        List<Good> goodsList = new ArrayList<>();
        try (Connection conn = provider.getConnection();
             PreparedStatement st = conn.prepareStatement(SELECT_ALL_GOODS);
             ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                goodsList.add(new Good(rs.getLong(ID_COLUMN), rs.getString(NAME_COLUMN), rs.getDouble(PRICE_COLUMN)));
            }
        }
        return goodsList;
    }

    /**
     * Creates PreparedStatement with the transferred arguments
     * @param con the Connection
     * @param param param for SQL statement
     * @param sql the SQL Statement
     * @return the {@link PreparedStatement}
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    private PreparedStatement createPreparedStatement(Connection con, Object param, String sql) throws SQLException {
        PreparedStatement ps = con.prepareStatement(sql);
        ps.setObject(1, param);
        return ps;
    }
}
