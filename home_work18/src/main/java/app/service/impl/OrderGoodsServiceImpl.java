package app.service.impl;

import app.dao.OrderGoodsDao;
import app.dao.impl.OrderGoodsDaoImpl;
import app.entity.Good;
import app.entity.OrderGoods;
import app.service.ContextUtil;
import app.service.ServiceException;
import app.utils.GoodsUtil;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderGoodsServiceImpl {
    private OrderGoodsDao orderGoodsDao;
    private GoodsServiceImpl goodsService;

    public OrderGoodsServiceImpl(){
        orderGoodsDao = (OrderGoodsDaoImpl) ContextUtil.getContext().getBean("orderGoodsDao");
        goodsService = (GoodsServiceImpl)ContextUtil.getContext().getBean("goodsService");
    }

    /**
     * Added product by name to the OrderGoods db table
     * @param name name of product to be added
     * @param orderId id of order in which the product included
     */
    public void add(String name, Long orderId) {
        try {
            Good good = (goodsService.getGood(GoodsUtil.getName(name)));
            orderGoodsDao.addToOrderGood(orderId, good.getId());
        } catch (SQLException e) {
            throw new ServiceException("Exception with basket. Try again later", e);
        }
    }

    /**
     * Returns the map contains name of item, its price and number of it in order
     * @param orderId id of order for map building
     * @return the map
     */
    public Map<String, Integer> getOrderedGoods(Long orderId) {
        Map<String, Integer> map = new HashMap<>();
        List<Good> orderedGoods = getGoods(orderId);
        for(Good good : orderedGoods) {
            String item = (good.getName() + " (" + good.getPrice() + " $)");
            int value = 1;
            if (map.containsKey(item)) {
                value = map.get(item) + 1;
                map.remove(item);
            }
            map.put(item, value);
        }
        return map;
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred id
     * @param orderId id of order for list building
     * @return the list
     */
  public List<Good> getGoods(Long orderId) {
      List<Good> orderedGoods = new ArrayList<>();
      try {
          List<OrderGoods> inCurrentOrder = orderGoodsDao.getByOrderId(orderId);

          for (OrderGoods current : inCurrentOrder) {
              orderedGoods.add(goodsService.getGood(current.getGoodId()));
          }
      } catch (SQLException e) {
          throw new ServiceException("Exception in data base connection,", e);
      }
      return orderedGoods;
  }
}
