package app.service.impl;

import app.dao.GoodDao;
import app.dao.impl.GoodDaoImpl;
import app.entity.Good;
import app.service.ContextUtil;
import app.service.ServiceException;
import java.sql.SQLException;
import java.util.List;

public class GoodsServiceImpl {
    private GoodDao goodDao;

    public GoodsServiceImpl() {
        goodDao = ((GoodDaoImpl) ContextUtil.getContext().getBean("goodDao"));
    }

    /**
     * @return list contains all Goods
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    public List<Good> getGoods() throws SQLException {
        return goodDao.getAllGoods();
    }


    /**
     * Returns the good by the transferred name
     * @param name good's name
     * @return the {@link Good}
     */
    public Good getGood(String name) {
        Good toReturn;
        try {
            toReturn = goodDao.getGood(name).orElseThrow(() -> new ServiceException("Order can't be created"));
        } catch (SQLException e) {
            throw new ServiceException("Order can't be created");
        }
        return toReturn;
    }


    /**
     * Returns the good by the transferred id
     * @param id good's id
     * @return the {@link Good}.
     */
    public Good getGood(Long id) {
        Good toReturn;
        try {
            toReturn = goodDao.getGood(id).orElseThrow(() -> new ServiceException("Order can't be created"));
        } catch (SQLException e) {
            throw new ServiceException("Order can't be created");
        }
        return toReturn;
    }
}
