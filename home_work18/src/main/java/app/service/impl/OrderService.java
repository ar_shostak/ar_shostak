package app.service.impl;

import app.dao.OrderDao;
import app.entity.Order;
import app.service.ContextUtil;
import app.service.ServiceException;
import java.sql.SQLException;
import java.util.Optional;

public class OrderService {
    OrderDao orderDao;

    public OrderService() {
        orderDao = (OrderDao) (ContextUtil.getContext().getBean("orderDao"));
    }

    /**
     * Return the Order by User's id. If it not exist, create a new order and the return
     * @param stringId user's id
     * @return the Order
     */
    public Order get(String stringId) {
        Long id = Long.parseLong(stringId);
        Order toReturn;
        try {
            Optional<Order> order = orderDao.getOrderByUserId(id);
            toReturn = order.orElseGet(() -> create(id));
        } catch (SQLException e) {
            throw new ServiceException("Order can't be created");
        }
        return toReturn;
    }

    /**
     * Create a new Order by the transferred user's id
     * @param id the user's id
     * @return created user
     */
    Order create(Long id) {
        try {
            orderDao.addToOrder(id);
            return orderDao.getOrderByUserId(id).orElseThrow(() -> new ServiceException("Order can't be created"));

        } catch (SQLException e) {
            throw new ServiceException("Unsuccessful order creation. Check out data base connection", e);
        }
    }

    /**
     * Clears basket when the order is finished
     */
    public void updateByUserId(double orderPrice, Long orderId) {
        orderDao.updateOrderById(orderPrice, orderId);
    }
}