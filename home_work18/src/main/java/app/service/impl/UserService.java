package app.service.impl;

import app.dao.UserDao;
import app.dao.impl.UserDaoImpl;
import app.entity.User;
import app.service.ContextUtil;
import app.service.ServiceException;
import java.sql.SQLException;
import java.util.Optional;

public class UserService{
    UserDao userDao;

    public UserService() {
        userDao = (UserDaoImpl) ContextUtil.getContext().getBean("userDao");
    }

    /**
     * Return the User by name. If he not exist, create new user and the return
     * @param name user's name
     * @return the User
     */
    public User get(String name){
        User toReturn;
        try {
            Optional<User> user = userDao.getUserByName(name);
            toReturn = user.orElseGet(() -> create(name));
        } catch (SQLException e) {
            throw new ServiceException("Order can't be created");
        }
        return toReturn;
    }

    /**
     * Create a new user by the transferred name
     * @param name the user's name
     * @return created user
     */
    public User create(String name){
        try {
            userDao.addUser(name, name);
            return userDao.getUserByName(name).orElseThrow(() -> new ServiceException("Order can't be created"));
        } catch(SQLException e) {
            throw new ServiceException("Order can't be created");
        }
    }
}
