package app.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
    private static final String CREATE_DB_URL = "jdbc:h2:mem:testdb;INIT=RUNSCRIPT FROM 'classpath:create_tables.sql'\\;RUNSCRIPT FROM 'classpath:populate.sql';DB_CLOSE_DELAY=-1";
    private static final String RETURN_DB_URL = "jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1";

    /**
     * Create instance of ConnectionProvider
     */
    public ConnectionProvider() {
        initBase();
    }

    /**
     * @return created connection
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(RETURN_DB_URL);
    }

    /**
     * Creates a connection, creates a database, and initializes its original data.
     */
    private void initBase() {
        try {
            Connection connection = DriverManager.getConnection(CREATE_DB_URL);
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException("Unable init base", e);
        }
    }
}