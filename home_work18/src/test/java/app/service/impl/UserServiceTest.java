package app.service.impl;

import app.dao.impl.UserDaoImpl;
import app.entity.User;
import app.service.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.SQLException;
import java.util.Optional;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserDaoImpl daoMock;

    @InjectMocks
    private UserService userService;

    @Test
    public void testGetUser() throws SQLException {
        //Given
        User expected = new User(1, "Archi", "112");
        when(daoMock.getUserByName("Archi")).thenReturn(Optional.of(expected));
        //When
        User actual = userService.get("Archi");
        //Then
        assertEquals(expected, actual);
    }

    @Test(expected = ServiceException.class)
    public void testGetUserUnsuccessful() throws SQLException {
        //Given
        when(daoMock.getUserByName("David")).thenReturn(Optional.empty());
        //When
        User actual = userService.get("David");
    }

    @Test
    public void testCreate() throws SQLException {
        //Given
        User expected = new User(3, "David", "111");
        when(daoMock.getUserByName("David")).thenReturn(Optional.of(expected));
        //When
        User actual = userService.get("David");
        //Then
        assertEquals(expected, actual);
    }

    @Test(expected = ServiceException.class)
    public void testUnsuccessfulCreate() throws SQLException {
        //Given
        when(daoMock.getUserByName("Dav")).thenReturn(Optional.empty());
        //When
        User actual = userService.get("Dav");
    }
}