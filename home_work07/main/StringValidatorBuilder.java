package mog.epam.java_course.home_work07;

public class StringValidatorBuilder extends ValidationSystem{

    /**
     * Creates and returns an object of String validation
     * @return an string validator
     */
    public Validator createValidator() {
        return new StringValidator();
    }
}
