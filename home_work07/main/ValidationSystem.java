package mog.epam.java_course.home_work07;

public abstract class ValidationSystem {

    /**
     * Verifies the passed data type
     * @param t an object for validation. Can be String or Integer type
     * If an object do not corresponds to given parameters it throws an ValidationFailedException
     */
    public static <T> void validate(T t) throws ValidationFailedException {
        Validator validator = configure(t).createValidator();
        validator.validate(t);
    }

    /**
    * Create the appreciative type builder object
    * @param t an object for validation
    *@return required Validator Builder
    */
    static <T> ValidationSystem configure(T t) {
        try {
            Integer i = Integer.parseInt(t.toString());
            return new IntegerValidatorBuilder();
        } catch (Exception e) {
            return new StringValidatorBuilder();
        }
    }

    /**
     * Method for overriding by the appropriate type builder
     * @return a validator of appreciative type
     */
    public abstract Validator createValidator();
}
