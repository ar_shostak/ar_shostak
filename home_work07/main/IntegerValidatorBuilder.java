package mog.epam.java_course.home_work07;

public class IntegerValidatorBuilder extends ValidationSystem {

    /**
     * Creates and returns an object of Integer validation
     * @return an integer validator
     */
    public Validator createValidator() {
        return new IntegerValidator();
    }
}
