package mog.epam.java_course.home_work07;

public class ValidationFailedException extends Exception {
    public ValidationFailedException() {
        super("Validation Faild!");
    }
}
