package mog.epam.java_course.home_work07;

public interface Validator<T> {

    /**
     * Method for overriding by the appropriate type validator
     * @param t an object for validation. Can be String or Integer type
     */
    public void validate(T t) throws ValidationFailedException;
}
