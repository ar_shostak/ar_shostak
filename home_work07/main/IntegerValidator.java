package mog.epam.java_course.home_work07;

public class IntegerValidator implements Validator<Integer>{

    /**
     * Checks whether number lies in the range from 1 to 10. It could throw ValidationFailedException
     * @param integer integer to check
     * @throws ValidationFailedException if argument does not match the accepted parametres
     */
    @Override
    public void validate(Integer integer) throws ValidationFailedException {
        if (!((integer > 0) & (integer <= 10))) {
            throw new ValidationFailedException();
        }
    }
}
