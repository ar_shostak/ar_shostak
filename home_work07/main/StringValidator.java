package mog.epam.java_course.home_work07;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator implements Validator<String> {

    /**
     * Checks whether a string begins with a upper case latter.It could throw ValidationFailedException
     * @param text string to check
     * @throws ValidationFailedException if a string does not begins with a upper case
     */
    @Override
    public void validate(String text) throws ValidationFailedException {
        Pattern pattern = Pattern.compile("^[A-Z].*");
        Matcher matcher = pattern.matcher(text);
        if (!matcher.matches()) {
            throw new ValidationFailedException();
        }
    }
}
