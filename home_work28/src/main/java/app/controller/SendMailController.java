package app.controller;

import app.entity.User;
import app.service.impl.EmailService;
import app.service.impl.OrderGoodsServiceImpl;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import static app.page_path.PagePath.LETTER_WAS_SENT;

@Controller
@RequestMapping({"/"})
public class SendMailController {
    public static final String BASKET = "order";
    private static final String CURRENT_USER = "currentUser";
    private static final String SEND_MAIL_URL = "/sendMailWithInlineImage";

    static { PropertyConfigurator.configure("src/logger.properties"); }
    private static final Logger logger = LoggerFactory.getLogger(InitializerController.class);
    private OrderGoodsServiceImpl service;
    private EmailService emailService;

    public SendMailController(OrderGoodsServiceImpl service, EmailService emailService) {
        this.service = service;
        this.emailService = emailService;
    }

    /**
     * Handles {@link HttpServlet} GET Method. Sends the letter with the order details
     * @param request the {@link HttpServletRequest}
     * @return the page contains information about successful sending
     * @throws MessagingException the {@link  MessagingException} throws by sendMailWithInline method
     */
    @RequestMapping(value = SEND_MAIL_URL, method = RequestMethod.GET)
    public ModelAndView sendMailWithInline(final HttpServletRequest request) throws MessagingException {
        logger.info("Get request to the URL " + SEND_MAIL_URL + " was accepted");
        HttpSession session = request.getSession();
        User user = (User)session.getAttribute(CURRENT_USER);

        emailService.sendMailWithInline(
            user.getUserName(),
            user.getEmail(),
            service.createPresentationParams((Map<String, Integer>)session.getAttribute(BASKET)),
            service.countPrice(user.getOrder().getGoods())
        );
        logger.info("The letter to the " + user.getEmail() + " was send");
        return new ModelAndView(LETTER_WAS_SENT.getURL());
    }
}
