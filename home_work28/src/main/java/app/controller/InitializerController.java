package app.controller;

import app.entity.Good;
import app.entity.Order;
import app.entity.User;
import app.page_path.PagePath;
import app.service.impl.GoodsServiceImpl;
import app.service.impl.OrderGoodsServiceImpl;
import app.service.impl.OrderServiceImpl;
import app.service.impl.UserServiceImpl;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("/")
public class InitializerController {
    static { PropertyConfigurator.configure("src/logger.properties"); }
    private static final Logger logger = LoggerFactory.getLogger(InitializerController.class);
    public static final String USER_NAME = "name";
    public static final String ALL_GOODS = "allGoodsList";
    private static final String BASKET = "order";
    private static final String TERM = "term";
    private static final String CURRENT_USER = "currentUser";
    private static final String ENTER_URL = "/createUserServlet";

    private List<Good> allGoodsList;
    private GoodsServiceImpl goodsService;
    private OrderGoodsServiceImpl orderGoodsService;
    private UserServiceImpl userService;
    OrderServiceImpl orderService;

    public InitializerController(GoodsServiceImpl goodsService, OrderServiceImpl orderService, OrderGoodsServiceImpl orderGoodsService, UserServiceImpl userService) {
        this.goodsService = goodsService;
        this.orderGoodsService = orderGoodsService;
        this.userService = userService;
        this.orderService = orderService;
    }

    /**
     * Transfers to the first page
     * @return the {@link ModelAndView}
     */
    @GetMapping()
    public ModelAndView redirectToFirstPage() {
        logger.info("User redirected to the start page");
        return new ModelAndView(PagePath.HOME_PAGE.getURL());
    }

    /**
     * Handles http post request
     * @param request the {@link HttpServletRequest}
     * @param userName the user's name
     * @return the {@link ModelAndView} for the goods select page
     */
    @PostMapping(value = ENTER_URL)
    public ModelAndView createUser(@RequestParam(USER_NAME) String userName, HttpServletRequest request) {
        logger.debug("Post request was accepted from " + userName);
        allGoodsList = goodsService.getGoods();
        ModelAndView model;
        if ((userName.equals("")) || (request.getParameter(TERM) == null)) {
            logger.info("User redirected to the terms error page");
            model = new ModelAndView(PagePath.TERMS_ERROR.getURL());

        } else {
            User user = userService.getUser(userName);
            Order order = orderService.get(String.valueOf(user.getId()));
            user.setOrder(order);
            addSessionAttributes(user, request);
            logger.info("User with name " + userName + " began the session" );
            model = createView(user);
        }
        return model;
    }

    /**
     * Creates the {@link ModelAndView} for the goods select page
     * @param user the current user
     * @return the {@link ModelAndView}
     */
    private ModelAndView createView(User user) {
        logger.debug("A model with path " + PagePath.ADD.getURL() + " start to be created" );
        ModelAndView model = new ModelAndView(PagePath.ADD.getURL()).addObject(ALL_GOODS, allGoodsList);
        model.addAllObjects(Stream.of(new Object[][]{
                {USER_NAME, user.getUserName()},
                {BASKET, orderGoodsService.getOrderedGoods(user.getOrder().getGoods())},
        }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])));
        logger.debug("A model with path " + PagePath.ADD.getURL() + "created successfully" );
        return model;
    }

    /**
     * Adds attributes to the session
     * @param user the current user
     * @param request the {@link HttpServletRequest}
     */
    private void addSessionAttributes(User user, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute(ALL_GOODS, allGoodsList);
        session.setAttribute(CURRENT_USER, user);
    }
}