package app.entity;

import javax.persistence.*;
import java.util.*;

/**
 * Class represents a bin to describe an order
 */
@Entity
@Table(name = "ORDERS")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "orderId", nullable = false)
    private long id;

    @Column(name = "userId")
    private long userId;

    @Column(name = "totalPrice")
    private double totalPrice;

    @OneToMany(cascade = {CascadeType.ALL },fetch = FetchType.EAGER)
    @JoinTable(name = "ORDER_GOODS", joinColumns = { @JoinColumn(name = "orderId")},  inverseJoinColumns = @JoinColumn(name = "goodId") )
    private List<Good> goods = new LinkedList<>();

    public void addGood(Good good) {
        goods.add(good);
    }

    public void removeGood(Good good) {
        goods.remove(good);
    }

    public Order(){}

    public Order(long id, long userId, double totalPrice) {
        this.id = id;
        this.userId = userId;
        this.totalPrice = totalPrice;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                userId == order.userId &&
                goods == order.goods &&
                Double.compare(order.totalPrice, totalPrice) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, totalPrice, goods);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", userId=" + userId +
                ", totalPrice=" + totalPrice +
                '}';
    }
}

