package app.dao.impl;

import app.dao.OrderDao;
import app.entity.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
@Lazy
public class OrderDaoImpl implements OrderDao {
    private static final String SELECT_ORDER_BY_USER = "from Order where userId = :userId";
    private static final String UPDATE_ORDER = "update Order set userId = :userId";
    private static final String USER_ID = "userId";

    private SessionFactory factory;

    public OrderDaoImpl(SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Returns the order by the transferred user's id
     * @param userId user's id
     * @return the {@link Order}
     */
    @Override
    public Optional<Order> getOrderByUserId(Long userId){
        Session session = factory.getCurrentSession();
        return session.createQuery(SELECT_ORDER_BY_USER).setParameter(USER_ID, userId).uniqueResultOptional();
    }

    /**
     * Add the order to Order table
     * @param order the order
     */
    @Override
    public void addToOrder(Order order) {
        Session session = factory.getCurrentSession();
        session.save(order);
    }

    /**
     * Update user's Order with transferred price
     * @param totalPrice price to be assigned to the order
     * @param userId id of user to update
     */
    @Override
    public boolean updateOrderById(double totalPrice, Long userId) {
        Session session = factory.getCurrentSession();
        session.createQuery(UPDATE_ORDER).setParameter(USER_ID, userId).executeUpdate();
        return true;
    }

    /**
     * Update user's Order with transferred price
     * @param order the order
     */
    @Override
    public boolean updateOrder(Order order) {
       Session session = factory.getCurrentSession();
       order = Optional.ofNullable(session.get(Order.class, order.getId())).orElseThrow(() -> new RuntimeException("Can't get order"));
       session.saveOrUpdate(order);
       return true;
    }
}

