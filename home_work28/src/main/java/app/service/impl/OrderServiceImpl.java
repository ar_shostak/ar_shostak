package app.service.impl;

import app.dao.OrderDao;
import app.entity.Order;
import app.service.ServiceException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Service
@Lazy
public class OrderServiceImpl {
    private OrderDao orderDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * Return the Order by User's id. If it not exist, create a new order and the return
     * @param stringId user's id
     * @return the Order
     */
    @Transactional
    public Order get(String stringId) {
        Long id = Long.parseLong(stringId);
        Optional<Order> order = orderDao.getOrderByUserId(id);
        return order.orElseGet(() -> create(id));
    }

    /**
     * Create the new Order
     * @param userId the user's id
     * @return created order
     */
    @Transactional
    Order create(Long userId){
        Order order = new Order();
        order.setUserId(userId);
        order.setTotalPrice(0.0);
        orderDao.addToOrder(order);
        return orderDao.getOrderByUserId(userId).orElseThrow(() -> new ServiceException("Order can't be created"));
    }

    /**
     * Clears basket when the order is finished
     */
    @Transactional
    public boolean updateByUserId(double orderPrice, Long orderId) {
        return orderDao.updateOrderById(orderPrice, orderId);
    }
}