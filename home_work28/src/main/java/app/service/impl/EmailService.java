package app.service.impl;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Date;
import java.util.List;

@Service
public class EmailService {
    private static final String USER_NAME = "name";
    private static final String SUBSCRIPTION_DATE = "subscriptionDate";
    private static final String PRICE = "price";
    private static final String GOODS_LIST = "goodsList";
    private static final String IMAGE_NAME = "imageResourceName";
    private static final String ENCODING = "UTF-8";
    private static final String IMAGE = "src/main/webapp/WEB-INF/view/message/java.jpeg";
    private static final String MAIL_TEXT_FILE = "message/orderMail";
    private static final String FROM =  "thymeleaf@example.com";
    private static final String SUBJECT =    "Example HTML email with inline image";

    private JavaMailSender mailSender;
    private SpringTemplateEngine emailTemplateEngine;

    public EmailService(JavaMailSender mailSender, SpringTemplateEngine emailTemplateEngine){
        this.mailSender = mailSender;
        this.emailTemplateEngine = emailTemplateEngine;
    }

    /**
     * Sends mail containing info about order
     * @param userName the userName
     * @param email the email to send a letter
     * @param goodsList chosen goods
     * @param price the price of the order
     * @throws MessagingException throws by MimeMessageHelper class
     */
    public void sendMailWithInline(
        final String userName, final String email, final List<String> goodsList, String price) throws MessagingException {

        final Context context = new Context();
        context.setVariable(USER_NAME, userName);
        context.setVariable(SUBSCRIPTION_DATE, new Date());
        context.setVariable(PRICE, price);
        context.setVariable(GOODS_LIST, goodsList);
        context.setVariable(IMAGE_NAME, IMAGE);

        final MimeMessage mimeMessage = mailSender.createMimeMessage();
        final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, ENCODING);
        message.setSubject(SUBJECT);
        message.setFrom(FROM);
        message.setTo(email);

        final String htmlContent = emailTemplateEngine.process(MAIL_TEXT_FILE, context);
        message.setText(htmlContent, true);

        message.addInline(IMAGE_NAME, new File(IMAGE));

        mailSender.send(mimeMessage);
    }
}
