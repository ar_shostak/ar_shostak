package app.service.impl;

import app.dao.DAOProvider;
import app.entity.Good;
import app.service.ServiceException;
import app.service.ServiceProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class OrderGoodsServiceImplTest {
    @Mock
    private DAOProvider daoMock;
    @Mock
    private OrderGoodsServiceImpl orGoodsServiceMock;
    @Mock
    private ServiceProvider serviceProviderMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getOrderedGoods() throws ServiceException {
        List<Good> goodsList = Arrays.asList(new Good(1L, "Toy", 2.0),
                                            new Good(2L, "Toy", 2.0),
                                            new Good(3L, "House", 255.0),
                                            new Good(4L, "Car", 80.0));

        Map<String, Integer> expectedMap = new HashMap<>();
        expectedMap.put("Toy (2.0 $)", 2);
        expectedMap.put("Toy (255.0 $)", 1);
        expectedMap.put("Toy (80.0 $)", 1);

        when(serviceProviderMock.getOrderGoodsService()).thenReturn(orGoodsServiceMock);
        orGoodsServiceMock.setDao(daoMock);
        when(orGoodsServiceMock.getGoods(2L)).thenReturn(goodsList);
        when(orGoodsServiceMock.getOrderedGoods(2L)).thenReturn(expectedMap);
        //When
        Map<String, Integer> actual = serviceProviderMock.getOrderGoodsService(). getOrderedGoods(2L);
        //Then
        assertEquals(expectedMap, actual);
    }

}