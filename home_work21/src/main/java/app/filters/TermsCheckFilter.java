package app.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter(urlPatterns = "/createUserServlet")
public class TermsCheckFilter implements Filter{
    private static final String TERM = "term";
    public static final String BASKET = "order";
    public static final String PAGE_TERMS_ERROR = "http://localhost:8080/Online-shop2/WEB-INF/view/errors/termsError.html";

    /**
     * Initialize filter in filterListener
     */
    public void init(FilterConfig config) {
    }

    /**
     * Checks if user's terms have been accepted.If yes, calls user creation, otherwise redirect to the error page
     * @param request  the {@link ServletRequest} contains user's name and field for terms accept
     *  transferred from the start(default) HTML page
     * @param response the {@link ServletResponse}
     * @throws IOException      thrown when occur exception in redirecting
     * @throws ServletException thrown when occur exception in redirecting
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        if (request.getParameter(TERM) == null) {
           request.getRequestDispatcher(PAGE_TERMS_ERROR).forward(request, response);
        } else {
           chain.doFilter(request, response);
        }
    }

}
