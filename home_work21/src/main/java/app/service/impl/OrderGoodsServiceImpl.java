package app.service.impl;

import app.dao.DAOProvider;
import app.entity.Good;
import app.entity.OrderGoods;
import app.service.ServiceException;
import app.utils.GoodsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.sql.SQLException;
import java.util.*;

@Service
@Lazy
public class OrderGoodsServiceImpl {
    public static final String PRINT_GOODS = "goodsList";
    public static final String PRICE = "price";

    private DAOProvider dao;

    /**
     * @param dao the {@link DAOProvider}
     */
    @Autowired
    public void setDao(DAOProvider dao) {
        this.dao = dao;
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred id
     * @param orderId id of order for list building
     * @return the list
     * An exception that provides information on a database access
     * error or other errors.
     */
    public List<Good> getGoods(Long orderId) throws ServiceException {
        List<Good> orderedGoods;
        try {
            List<OrderGoods> inCurrentOrder = dao.getOrderGoodsDao().getByOrderId(orderId);
            orderedGoods = getGoodsInCurrentOrder(inCurrentOrder);

        } catch (SQLException e) {
            throw new ServiceException("Exception in data base connection,", e);
        }
        return orderedGoods;
    }

    /**
     * Returns the map contains name of item, its price and number of it in order
     * @param orderId id of order for map building
     * @return the map
     * An exception that provides information on a database access
     * error or other errors.
     */
    public Map<String, Integer> getOrderedGoods(Long orderId) throws ServiceException {
        Map<String, Integer> map = new HashMap<>();
        List<Good> orderedGoods = getGoods(orderId);
        for(Good good : orderedGoods) {
            String item = (good.getName() + " (" + good.getPrice() + " $)");
            int value = 1;
            if (map.containsKey(item)) {
                value = map.get(item) + 1;
                map.remove(item);
            }
            map.put(item, value);
        }
        return map;
    }

    /**
     * Added product by name to the OrderGoods db table
     * @param name name of product to be added
     * @param orderId id of order in which the product included
     * An exception that provides information on a database access
     * error or other errors.
     */
    public void add(String name, Long orderId) throws ServiceException {
        try {
            Good good = dao.getGoodDao().getGood(GoodsUtil.getName(name));
            dao.getOrderGoodsDao().addToOrderGood(orderId, good.getId());
        } catch (SQLException e) {
            throw new ServiceException("Exception with basket. Try again later", e);
        }
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred OrderGoods list
     * @param goodsInCurrentOrder the OrderGoods list for good list creation
     * @return the list
     * An exception that provides information on a database access
     * error or other errors.
     */
    private List<Good> getGoodsInCurrentOrder(List<OrderGoods> goodsInCurrentOrder) throws SQLException {
        List<Good> orderedGoods = new ArrayList<>();
        for (OrderGoods current : goodsInCurrentOrder) {
            orderedGoods.add(dao.getGoodDao().getGood(current.getGoodId()));
        }
        return orderedGoods;
    }

    /**
     * Create map with parameters for print check
     * @param basket the map contains goods
     * @return the map with parameters
     */
    @ModelAttribute("goods")
    public List<String> createPresentationParams(@org.jetbrains.annotations.NotNull Map<String, Integer> basket) {
        List<String>params = new ArrayList<>();
        int i = 1;
        for (Map.Entry<String, Integer> pair : basket.entrySet()) {
            params.add(i + ") "+ pair.getKey() + " x " + pair.getValue());
            i += 1;
        }
        return params;
    }

    /**
     * Counts total price of order by id
     * @param orderId the order id
     * @return the price
     * @throws ServiceException An exception that provides information on a database access
     * error or other errors.
     */
    public String countPrice(long orderId) throws ServiceException {
        double orderPrice = GoodsUtil.countTotalPrice(getGoods(orderId));
        return String.format("%.1f", orderPrice);
    }
}
