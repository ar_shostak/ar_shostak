package app.entity;

import app.service.ServiceException;
import app.service.impl.OrderGoodsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;


public class Basket{

private Map<String, Integer> basket;
private static Basket instance;
private Long orderId;
private OrderGoodsServiceImpl service;


@Autowired
public OrderGoodsServiceImpl setService(OrderGoodsServiceImpl service){
    this.service = service;
    return service;
}

private Basket(Long orderId) throws ServiceException {
        this.orderId = orderId;
        basket = service.getOrderedGoods(orderId);
        }

/**
 * @return the basket of current user
 */
public static Basket getBasket() {
        return instance;
        }

/**
 * @return the basket of current user. Is the basket is empty, creates new basket
 * @param orderId order for basket creation
 */
public static Basket getBasket(Long orderId) throws ServiceException {
        if (instance == null) {
        instance = new Basket(orderId);
        }
        return instance;
        }

/**
 * Added chosen item to basket
 * @param item to add
 */
public void toBasket(String item) throws ServiceException {
        service.add(item, orderId);
        basket = service.getOrderedGoods(orderId);
        }

/**
 * Used for clearing the basket
 */
public void clear(){
        instance = null;
        }

/**
 * @return the map contains chosen goods
 */
public Map<String, Integer> getGoods() {
        return basket;
        }
}