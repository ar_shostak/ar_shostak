package app.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("daoProvider")
public class DAOProvider {
    public static UserDao userDao;
    public static GoodDao goodDao;
    public static OrderDao orderDao;
    public static OrderGoodsDao orderGoods;

    /**
     * The constructor establishes a connection and creates the provider object
     */
    private DAOProvider(){}

    @Autowired
    public void setOrderDao(OrderDao orderDao) {
        DAOProvider.orderDao = orderDao;
    }

    @Autowired
    public void setUserDao(UserDao userDao) {
        DAOProvider.userDao = userDao;
    }

    @Autowired
    public void setGoodDao(GoodDao goodDao) {
        DAOProvider.goodDao = goodDao;
    }

    @Autowired
    public void setOrderGoodsDao(OrderGoodsDao orderGoodsDao) {
        DAOProvider.orderGoods = orderGoodsDao;
    }

    /**
     * @return the GoodDao object
     */
    public UserDao getUserDao(){
       return userDao;
    }

    /**
     * @return the GoodDao object
     */
    public GoodDao getGoodDao(){
        return goodDao;
    }

    /**
     * @return the OrderDao object
     */
    public OrderDao getOrderDao(){
        return orderDao;
    }

    /**
     * @return the OrderGoodsDao object
     */
    public OrderGoodsDao getOrderGoodsDao(){
        return orderGoods;
    }

}
