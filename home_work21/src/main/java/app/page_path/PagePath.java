package app.page_path;

/**
 * This {@link Enum} encapsulate paths used in application
 */
public enum PagePath {
    ADD("/pages/addItem"),
    PRINT_CHECK("/pages/printCheck"),
    HOME_PAGE("/pages/homePage"),
    COMPLETIVE_SERVLET("/complete"),
    TERMS_ERROR("/errors/termsError"),
    EMPTY_BASKET_ERROR("/errors/emptyBasketError");

    private final String path;

    PagePath(String path) {
        this.path=path;
    }

    public String getPath() {
        return path;
    }
}
