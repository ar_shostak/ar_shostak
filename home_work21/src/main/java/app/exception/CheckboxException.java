package app.exception;

public class CheckboxException extends Exception{

    public CheckboxException(Exception e){
        super(e);
    }
    public CheckboxException(String str){
        super(str);
    }

}
