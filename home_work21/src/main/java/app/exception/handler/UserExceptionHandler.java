package app.exception.handler;

import app.exception.CheckboxException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class UserExceptionHandler  {
    @ExceptionHandler(CheckboxException.class)
    public ModelAndView handleUserNotFoundException(final CheckboxException exception) {
        final ModelAndView content = new ModelAndView("errors/termsError");
        content.addObject("text", exception.getMessage());
        return content;
    }
}
