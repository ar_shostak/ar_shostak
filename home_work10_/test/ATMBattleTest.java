package mog.epam.java_course.home_work10;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class ATMBattleTest {

    @Test
    public void testUpperBound() {
        ATMBattle battle = new ATMBattle();
        battle.main(new String[] {"50", "1000", "50", "2000"});
        BigDecimal actual = battle.card.getBalance();
        BigDecimal expected = BigDecimal.valueOf(Long.parseLong("1000"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testLowerBound() {
        ATMBattle battle = new ATMBattle();
        battle.main(new String[] {"50", "2000", "50", "1000"});
        BigDecimal actual = battle.card.getBalance();
        BigDecimal expected = BigDecimal.valueOf(Long.parseLong("0"));
        Assert.assertEquals(expected, actual);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testNullArg() {
        ATMBattle battle = new ATMBattle();
        battle.main(new String[]{null, "2000", "50", "1000"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongArg() {
        ATMBattle battle = new ATMBattle();
        battle.main(new String[]{"Vasya", "2000", "50", "1000"});
        BigDecimal actual = battle.card.getBalance();
    }
}