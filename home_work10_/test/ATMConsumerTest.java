package mog.epam.java_course.home_work10;

import mog.epam.java_course.home_work04.task1.Atm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ATMConsumerTest {
    ATMConsumer atm;
    Card cardCredit;

    @Before
    public void initCards() {
        atm = new ATMConsumer("BPS");
        cardCredit = new CreditCard("David", new BigDecimal(200.0));
    }

    @After
    public void clear() {
        atm = null;
        cardCredit = null;
    }

    @Test
    public void testATMConstrWithOneArg() {
        Atm actual = new Atm("BelarusBank");
        String expected = "BelarusBank";
        assertEquals(expected, actual.getOwner());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testATMConstrWithOneWrongArg() {
        Atm actual = new Atm("234");
    }

    @Test
    public void testWithdraw() {
        atm.acceptCard(cardCredit);
        cardCredit.withdraw(new BigDecimal(100.0));
        BigDecimal actual = cardCredit.getBalance();
        BigDecimal expected = new BigDecimal(100.0);
        assertEquals(expected, actual);
    }
}
