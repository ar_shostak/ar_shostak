package mog.epam.java_course.home_work10;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ATMProducerTest {
    ATMProducer atm;
    Card cardCredit;

    @Before
    public void initCards() {
        atm = new ATMProducer("BPS");
        cardCredit = new CreditCard("David", new BigDecimal(200.0));
    }

    @After
    public void clear() {
        atm = null;
        cardCredit = null;
    }

    @Test
    public void testATMConstrWithOneArg() {
        ATMProducer actual = new ATMProducer("BelarusBank");
        String expected = "BelarusBank";
        assertEquals(expected, actual.getOwner());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testATMConstrWithOneWrongArg() {
        ATMProducer actual = new ATMProducer("234");
    }

    @Test
    public void testAddTo() {
        atm.acceptCard(cardCredit);
        cardCredit.addTo(new BigDecimal(100.0));
        BigDecimal actual = cardCredit.getBalance();
        BigDecimal expected = new BigDecimal(300.0);
        assertEquals(expected, actual);
    }
}
