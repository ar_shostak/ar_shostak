package mog.epam.java_course.home_work10;

import mog.epam.java_course.home_work04.task1.Card;
import mog.epam.java_course.home_work04.task1.CreditCard;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class CreditCardTest {
    Card cardCredit;

    @Before
    public void initCards() {
        cardCredit = new CreditCard("David", new BigDecimal(200.0));
    }

    @After
    public void clear() {
        cardCredit = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCardConstrWithOneWrongArg() {
        Card actual = new CreditCard("123");
    }

    @Test
    public void testCardConstrWithOneArg() {
        Card actual = new CreditCard("Paul");
        String expected = "Paul";
        assertEquals(expected, actual.getHolderName());
    }

    @Test
    public void testAddToCreditCard() {
        cardCredit.addTo(new BigDecimal(100.0));
        BigDecimal actual = cardCredit.getBalance();
        BigDecimal expected = new BigDecimal(300.0);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawFromCreditCard() {
        BigDecimal actual = cardCredit.withdraw(new BigDecimal(50.0));
        BigDecimal expected = new BigDecimal(50.0);
        assertEquals(expected, actual);
    }

}
