package mog.epam.java_course.home_work10;

import java.math.BigDecimal;

/**
 * Using the card transferred to him, it allows to carry out replenishment operation
 */
public class ATMProducer implements Runnable, ATM {

    /**
     * This field contains information about bank-owner of ATM
     */
    private String owner;

    /**
     * The card for operations
     */
    private Card card;

    /**
     * This field is used for interrupted current thread
     */
    private boolean interrupted = false;

    /**
     * Sets the amount to be added
     */
    private BigDecimal amount;

    /**
     * Sets the time interval for repeating the add operation
     */
    private int frequency;

    /**
     * Constructor to create a new ATMProducer of given bank
     *
     * @param owner bank owner of ATM consists of letters only
     */
    public ATMProducer(String owner) {
        if (!owner.matches("[A-Za-z]+")) {
            throw new IllegalArgumentException("Bad input.");
        }
        this.owner = owner;
    }

    /**
     * @return the name of ATM's owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Assign the value for owner field
     *
     * @param owner this value will be assigned as a class field value
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * Transfers to ATM the card for operations
     *
     * @param card card for operations
     */
    public void acceptCard(Card card) {
        this.card = card;
    }

    /**
     * Take from ATM the card for operations
     *
     * @return the card that was used with ATM
     */
    public Card returnCard() {
        return card;
    }

    /**
     * Add to the balance the value of argument
     * @param amount value will be added to the current balance
     * @param card   card for operations
     */
    public void addTo(Card card, BigDecimal amount) {
        card.addTo(amount);
    }

    /**
     * Is used to terminate the operation of the ATM
     */
    @Override
    public void interrupt() {
        interrupted = true;
    }

    /**
     * Sets the sum of operation
     * @param amount the sum of operation
     */
    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Sets the frequency of operation
     * @param frequency the frequency of operation
     */
    @Override
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    /**
     * Runs the ATM to add a specified sum of money at specified intervals of time.
     * Performs the operation until the interrupt command will be received
     */
    public void run() {
        while (interrupted == false) {
            try {
                ATMBattle.START.await();
                synchronized (card) {
                    addTo(this.card, amount);
                }
                System.out.println(amount + " dollars were added, current balance = " + card.getBalance() + " " + Thread.currentThread().getName());
                Thread.sleep(frequency);
            } catch (InterruptedException e) {

            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ATMProducer atm = (ATMProducer) o;
        if (owner == null) {
            if (atm.owner != null) {
                return false;
            } else if (!owner.equals(atm.owner)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((owner == null) ? 0 : owner.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ATMProducer{" +
                "Owner=" + owner + '}';
    }
}
