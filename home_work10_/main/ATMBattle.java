package mog.epam.java_course.home_work10;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ATMBattle {
    static final CountDownLatch START = new CountDownLatch(10);
    static Card card;
    /**
     *The program creates 5 ATMConsumers  and 5 ATMProducers and performs the corresponding
     * operations with the transferred card. If the balance reaches 0 or 1000, their work stops
     * and the balance value displayed.
     * The args[] array is stored: amount - the number of repetitions for ATMProducer(args [0][1]) and for
     * ATMConsumer(args[2][3])
     */
    public static void main(String[] args) {
        card = new CreditCard("Artyom", BigDecimal.valueOf(500));
        Runnable[] atmList = new Runnable[10];
        atmList = fillTheAtmList(atmList, card, args);
        ExecutorService service = Executors.newFixedThreadPool(10);
        for (int i = 0; i < atmList.length; i++) {
            service.execute(array[i]);
            START.countDown();
        }
        while(true) {
            if (checkBalanceForReachingLimit(card) == true) {
                for (int i = 0; i < atmList.length; i++) {
                    ATM atm = (ATM) atmList[i];
                    atm.interrupt();
                }
                service.shutdownNow();
                break;
            }
        }
    }

    /**
     * Checks the balance if it is not reach its limits. Return
     * @param card card for balance checking
     * @return returns true if balance has reached its limits
     */
    private static boolean checkBalanceForReachingLimit(Card card) {
        boolean toReturn = false;
        if (((card.getBalance().compareTo((BigDecimal.ZERO)) == 0) | ((card.getBalance().compareTo(BigDecimal.valueOf(Long.parseLong("1000")))) == 0))) {
            System.out.println("The balance is " + card.getBalance());
            toReturn = true;
        }
        return toReturn;
    }

    /**
     * Fills the transferred array with 5 ATMProducers and 5 ATMConsumers
     * @param array the array to be field
     * @param card the card with which the ATM will work
     * @return the filled array
     */
    private static Runnable[] fillTheAtmList(Runnable[] atmList, Card card, String[] args) {
        int j = 0;
        while (j < 10) {
            ATMProducer producer = new ATMProducer("BPS");
            producer.acceptCard(card);
            try {
                producer.setAmount(BigDecimal.valueOf(Long.parseLong(args[0])));
                producer.setFrequency(Integer.parseInt(args[1]));
                array[j] = producer;
                ATMConsumer consumer = new ATMConsumer("Belagroprom");
                consumer.acceptCard(card);
                consumer.setAmount(BigDecimal.valueOf(Long.parseLong(args[2])));
                consumer.setFrequency(Integer.parseInt(args[3]));
                array[j + 1] = consumer;
                j += 2;
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Wrong arguments. Please check your input");
            }
        }
        return atmList;
    }
}
