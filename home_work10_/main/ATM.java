package mog.epam.java_course.home_work10;

import java.math.BigDecimal;

public interface ATM {

    /**
     * Is used to terminate the operation of the ATM
     */
    public void interrupt();

    /**
     * Sets the sum of operation
     * @param amount the sum of operation
     */
    public void setAmount(BigDecimal amount);

    /**
     * Sets the frequency of operation
     * @param frequency the frequency of operation
     */
    public void setFrequency(int frequency);
}
