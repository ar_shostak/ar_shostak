package mog.epam.java_course.home_work06;

import java.util.*;

public class TextLogic {
    private static final String PUNCTUATION_UNITS = "\\W";
    private static final String WHITESPACE = " ";

    /**
     * Remove all punctuations literals
     * @param text text for cleaning from punctuations literals
     * @return text in lower case without punctuation
     */
    public static String removePunctuation(String text) {
        String toReturn = text.replaceAll(PUNCTUATION_UNITS, WHITESPACE);
        return toReturn.toLowerCase();
    }

    /**
     * Build an array containing all words from argument String
     * @param text text for building an array of words
     * @return list containing all words from the text
     */
    public static List<String> split(String text) {
       String[] newArray = text.split(WHITESPACE);
        List<String> array = new ArrayList<String>();
        for (int i = 0; i < newArray.length; i++) {
            if (!newArray[i].equals("")) {
                array.add(newArray[i]);
            }
        }
        return array;
    }

    /**
     * Groups words by first letter and build a dictionary which indicates how many times the word is repeated in the text
     * @param wordsList is List that contains String
     * @return created dictionary
     */
    public static Map<String, Integer> buildVocabulary(List<String> wordsList) {
        Collections.sort(wordsList);
        Map<String, Integer> wordMap = new TreeMap<String, Integer>();

        for (int i = 0; i < wordsList.size(); i++) {
            String str = wordsList.get(i);
            wordMap.put(str, (wordMap.get(str) == null ? 1 : (wordMap.get(str) + 1)));
        }
        return wordMap;
    }

    /**
     * Print the dictionary
     * @param word a dictionary containing words
     */
    public static void print(Map<String, Integer> word) {
        boolean isFirst = true;
        int flag = 0;
        char ch = '*';
        for (Map.Entry<String, Integer> entry : word.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if (isFirst == true) {
                ch = entry.getKey().charAt(0);
                isFirst = false;
                flag = 1;
            }
            if ((entry.getKey().charAt(0)) == ch) {
                if (flag == 1) {
                    System.out.printf(entry.getKey().toUpperCase().charAt(0) + "  " + "%s : %s\n", key, value);
                    flag = 0;
                } else {
                    System.out.printf("   " + "%s : %s\n", key, value);
                }
            } else {
                System.out.printf(entry.getKey().toUpperCase().charAt(0) + "  " + "%s : %s\n", key, value);
                ch = entry.getKey().charAt(0);
            }
        }
    }
}



