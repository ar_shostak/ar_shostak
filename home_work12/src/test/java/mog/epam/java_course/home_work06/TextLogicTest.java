package mog.epam.java_course.home_work06;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class TextLogicTest {

    @Test
    public void testRemovePunctuation() {
        String str = "Mother/ clean/ a, window!";
        String expected = "mother  clean  a  window ";
        String actual = TextLogic.removePunctuation(str);
        assertEquals(expected, actual);
    }

    @Test
    public void testSplit() {
        String str = "mother  clean  a  window ";
        List<String> expected= new ArrayList<>();
        expected.add("mother");
        expected.add("clean");
        expected.add("a");
        expected.add("window");
        List<String> actual = TextLogic.split(str);
        assertEquals(expected, actual);
    }

    @Test
    public void testbuildVocabulary() {
        Map<String, Integer> expected = new HashMap<>();
        expected.put("mother", 1);
        expected.put("clean", 1);
        expected.put("a", 2);
        expected.put("window", 1);

        List<String> forActual= new ArrayList<>();
        forActual.add("mother");
        forActual.add("clean");
        forActual.add("a");
        forActual.add("a");
        forActual.add("window");
        Map<String, Integer> actual = TextLogic.buildVocabulary(forActual);
        assertEquals(expected, actual);
    }
}
