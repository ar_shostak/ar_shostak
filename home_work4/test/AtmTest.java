package mog.epam.java_course.home_work04;

import mog.epam.java_course.home_work04.task1.Atm;
import mog.epam.java_course.home_work04.task1.Card;
import mog.epam.java_course.home_work04.task1.CreditCard;
import mog.epam.java_course.home_work04.task1.DebitCard;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class AtmTest {
    Atm atm;
    Card cardDebit;
    Card cardCredit;

    @Before
    public void initCards() {
        cardDebit = new DebitCard("Artyom", new BigDecimal(100.0));
        cardCredit = new CreditCard("David", new BigDecimal(200.0));
    }

    @After
    public void clear() {
        cardDebit = null;
        cardCredit = null;
    }

    @Test(expected = NumberFormatException.class)
    public void testCardConstrWithOneWrongArg() {
        Card actual = new DebitCard("123");
        String expected = "Exception expected";
        assertEquals(expected, actual.getHolderName());
    }

    @Test
    public void testCardConstrWithOneArg() {
        Card actual = new CreditCard("Paul");
        String expected = "Paul";
        assertEquals(expected, actual.getHolderName());
    }

    @Test
    public void testATMConstrWithOneArg() {
        Atm actual = new Atm("BelarusBank");
        String expected = "BelarusBank";
        assertEquals(expected, actual.getOwner());
    }

    @Test(expected = NumberFormatException.class)
    public void testATMConstrWithOneWrongArg() {
        Atm actual = new Atm("234");
        String expected = "Exception expected";
        assertEquals(expected, actual.getOwner());
    }

    @Test
    public void testAddToDebitCard() {
        cardDebit.addTo(new BigDecimal(100.0));
        BigDecimal actual = cardDebit.getBalance();
        BigDecimal expected = new BigDecimal(200.0);
        assertEquals(expected, actual);
    }

    @Test
    public void testAddToCreditCard() {
        cardCredit.addTo(new BigDecimal(100.0));
        BigDecimal actual = cardCredit.getBalance();
        BigDecimal expected = new BigDecimal(300.0);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawFromDebitCard() {
        BigDecimal actual = cardDebit.withdraw(new BigDecimal(50.0));
        BigDecimal expected = new BigDecimal(50.0);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawFromCreditCard() {
        BigDecimal actual = cardCredit.withdraw(new BigDecimal(50.0));
        BigDecimal expected = new BigDecimal(50.0);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawTooMuchFromCreditCard() {
        BigDecimal actual = cardCredit.withdraw(new BigDecimal(150.0));
        BigDecimal expected = new BigDecimal(150);
        assertEquals(expected, actual);
    }

    @Test
    public void testWithdrawTooMuchFromDebitCard() {
        BigDecimal actual = cardDebit.withdraw(new BigDecimal(150.0));
        BigDecimal expected = null;
        assertEquals(expected, actual);
    }
}
