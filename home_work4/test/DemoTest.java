package mog.epam.java_course.home_work04;

import mog.epam.java_course.home_work04.task2.Demo;
import mog.epam.java_course.home_work04.task2.SorterException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertArrayEquals;

public class DemoTest {
    Demo demo;

    @Before
    public void createArray() {
        demo = new Demo();
    }

    @After
    public void refreshArray() {
        demo = null;
    }

    @Test
    public void testMainWithBubbleSort() throws SorterException {
        Demo.main(new String[]{"-b"});
        int[] actual = demo.array;
        int[] expected  = new int[] {0, 1, 2, 3, 6, 8, 9, 34, 67};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void testMainWithSelectionSort() throws SorterException {
        Demo.main(new String[]{"-c"});
        int[] actual = demo.array;
        int[] expected  = new int[] {0, 1, 2, 3, 6, 8, 9, 34, 67};
        assertArrayEquals(expected, actual);
    }

    @Test(expected = SorterException.class)
    public void testMainNegativeCase() throws SorterException {
        Demo.main(new String[]{"-ds"});
        int[] actual = demo.array;
        int[] expected  = new int[] {0, 1, 2, 3, 6, 8, 9, 34, 67};
        assertArrayEquals(expected, actual);
    }
}
