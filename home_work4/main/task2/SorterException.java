package mog.epam.java_course.home_work04.task2;

public class SorterException extends Exception {

    public SorterException(String message) {
        super(message);
    }
}
