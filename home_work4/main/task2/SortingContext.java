package mog.epam.java_course.home_work04.task2;

/**
 * Sorts {@code integer} array with the transferred sorter object
 */
public class SortingContext {
    private Sorter sorter;

    /**
     * Public constructor that create a new SortingContext object uses transferred argument
     * @param sorter type of sorter to be used
     */
    public SortingContext(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * Assign the value for sorter field
     *@param sorter this value will be assigned as a class field value
     */
    public void setSorter(Sorter sorter) {
        this.sorter = sorter;
    }

    /**
     * @return the value of sorter field
     */
    public Sorter getSorter() {
        return sorter;
    }

    /**
     * Calls the sort method of sorter object
     * @param array {@code integer} array to be sort
     */
    public void execute(int[] array) {
        sorter.sort(array);
    }
}
