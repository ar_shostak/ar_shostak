package mog.epam.java_course.home_work04.task2;

/**
 * This interface provides a method for sorting an {@code array}
 * of integers in ascending order
 */
public interface Sorter {
    public void sort(int[] array);
}
