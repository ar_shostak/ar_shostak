package mog.epam.java_course.home_work04.task2;

/**
 * Implements Sorter interface through bubble sort
 */
public class BubbleSort implements Sorter {

    /**
     * Sorts the transferred array using the bubble sort method
     * @param array the inegers {@code array} to be sort
     */
    @Override
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int k = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = k;
                }
            }
        }
    }
}
