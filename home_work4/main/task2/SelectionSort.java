package mog.epam.java_course.home_work04.task2;

/**
 * Implements Sorter interface throught selection sort
 */
public class SelectionSort implements Sorter{

    /**
     * Sorts the transferred array using the selectionsort method
     * @param array the inegers {@code array} to be sort
     */
    @Override
    public void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min= j;
                }
            }
            int tmp = array[i];
            array[i] = array[min];
            array[min] = tmp;
        }
    }
}
