package mog.epam.java_course.home_work04.task2;

/**
 * Sorts {@code integer} array with the selected method. The sort method
 * is selected by a transferred constant
 */
public class Demo {
    static final String SORTER_TYPE_BUBBLE = "-b";
    static final String SORTER_TYPE_SELECTION = "-c";
    public static int[] array = new int[] {2, 1, 3, 8, 9, 0, 67, 34, 6};

    public static void main(String[] args) throws SorterException {
        Sorter sorter;
        String sorterType = args[0];
        if (sorterType.equals(SORTER_TYPE_BUBBLE)) {
            sorter = new BubbleSort();
        } else if(sorterType.equals(SORTER_TYPE_SELECTION)) {
            sorter = new SelectionSort();
        } else {
            throw new SorterException("No such sorter type! ");
        }
        SortingContext context = new SortingContext(sorter);
        context.execute(array);
    }
}
