package mog.epam.java_course.home_work04.task1;

import java.math.BigDecimal;

/**
 * The {@code CreditCard} class provides operations for add to,
 * withdraw and viewing balance. Balance stored in {@code BigDecimal}.
 * Debit card allow negative balance value
 */
public class CreditCard extends Card {

    /**
     * Public constructor that create a new card with only owner's name argument.
     * Default balance value is set to 0;
     * @param holderName card owner's name consists of letters only!
     */
    public CreditCard(String holderName) {
        super(holderName);
    }

    /**
     * Public constructor that create a new card with name and balance received as arguments.
     * Default balance value is set to 0;
     * @param holderName card owner's name consists of letters only!
     * @param balance grater than 0;
     */
    public CreditCard(String holderName, BigDecimal balance) {
        super(holderName, balance);
    }

    /**
     * Subtract from the balance the value of the amount. If amount is greate
     * than balance, balance value will be negative
     * @param amount value will be subtract from the current balance.
     * @return requested value of money
     */
    @Override
    public BigDecimal withdraw(BigDecimal amount) {
            balance = balance.subtract(amount);
            return amount;
    }

    /**
     * Add to the balance the value of argument
     * @param amount value will be added to the current balance
     */
    @Override
    public void addTo(BigDecimal amount) {
        balance = balance.add(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Card card = (Card) o;
        if (balance == null) {
            if (card.balance != null) {
                return false;
            } else if (!balance.equals(card.balance)){
                return false;
            }
        } if (!holderName.equals(card.holderName)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode())
                + ((holderName == null) ? 0 : holderName.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Credit Card{" +
                "holderName='" + holderName + '\'' +
                ", balance=" + balance +
                '}';
    }
}
