package mog.epam.java_course.home_work04.task1;

import java.math.BigDecimal;

/**
 * Using the card transferred to him, it allows to carry out replenishment / withdrawal operations
 */
public class Atm {

    private String owner;

    /**
     * Constructor to create a new ATM of given bank
     * @param owner bank owner of ATM consists of letters only
     */
    public Atm(String owner) {
        if (!owner.matches("[A-Za-z]+")) {
            throw new NumberFormatException("Bad input.");
        }
        this.owner = owner;
    }

    /**
     * @return the name of ATM's owner
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Assign the value for owner field
     *@param owner this value will be assigned as a class field value
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     * Subtract from the balance the value of the amount.
     * @param card card for operations
     * @param amount value will be subtract from the current balance.
     * @return can be null or the requested amount
     */
    public BigDecimal withdraw(Card card, BigDecimal amount) {
        return card.withdraw(amount);
    }

    /**
     * Add to the balance the value of argument
     * @param amount value will be added to the current balance
     * @param card card for operations
     */
    public void addTo(Card card, BigDecimal amount) {
        card.addTo(amount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Atm atm = (Atm) o;
        if (owner == null) {
            if (atm.owner != null) {
                return false;
            } else if (!owner.equals(atm.owner)){
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((owner == null) ? 0 : owner.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "ATM{" +
                "Owner=" + owner + '}';
    }
}
