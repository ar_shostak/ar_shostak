package mog.epam.java_course.home_work11;

import java.io.Serializable;
import java.util.Objects;

/**
 * Implements the Component interface, representing a file. It has fields name and extension. Supported serialization
 */
public class File implements Component, Serializable {
    static final long serialVersionUID = 42L;
    private String name;
    private String extension;

    public File(String name, String extension) {
        this.name = name;
        this.extension = extension;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Displays this object in the form for submission to the directory
     * @param i the number of points on which shifted representation of this object in the directory
     */
    public void print(int i) {
        for(int j = 0; j < i; j++) {
            System.out.print(" ");
        }
        System.out.println(name + "." + extension);
    }

    /**
     * Method has empty implementation. It needs for
     * interchangeability of file and folder
     */
    public void add(Component component) {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return name.equals(file.name) &&
                extension.equals(file.extension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, extension);
    }

    @Override
    public String toString() {
        return "File{" +
                "name='" + name + '\'' +
                ", extension='" + extension + '\'' +
                '}';
    }
}
