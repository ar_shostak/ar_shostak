package mog.epam.java_course.home_work11;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Implements the Component interface, representing a Folder. It has fields name and component list(complist).
 * It can contains another files or folders. Supported serialization
 */
public class Folder implements Component, Serializable {
    static final long serialVersionUID = 42L;
    private String name;
    private List<Component> compList;

    public Folder (String name, List<Component> compList) {
        this.name = name;
        this.compList = compList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Component> getCompList() {
        return compList;
    }

    public void setCompList(List<Component> compList) {
        this.compList = compList;
    }

    /**
     * Method for adding some component to this folder
     * @param component needs to add in current folder
     */
    public void add(Component component) {
        boolean bol = true;
        for (Component comp : compList) {
            if ((comp.getName().equals(component.getName()))) {
                bol = false;
                break;
            }
        }
        if (bol == true & !(component.getName().equals("root"))) {
            compList.add(component);
        }
    }

    /**
     * Displays this object in the form for submission to the directory
     * @param i the number of points on which shifted representation of this object in the directory
     */
    public void print(int i) {
        for(int j = 0; j < i; j++) {
            System.out.print(" ");
        }
        System.out.println(name);
        i = i + 4;
        for (Component comp : compList) {
            comp.print(i);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Folder folder = (Folder) o;
        return name.equals(folder.name) &&
                compList.equals(folder.compList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, compList);
    }

    @Override
    public String toString() {
        return "Folder{" +
                "name='" + name + '\'' +
                ", compList=" + compList +
                '}';
    }
}
