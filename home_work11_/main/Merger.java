package mog.epam.java_course.home_work11;

/**
 * Attaches path objects to the root directory
 */
public class Merger {
    static boolean isFind = false;

    /**
     * Method calls method for Component comparison and merging accepted Components
     * @param comp1 root Component
     * @param comp2 child Component
     * @return merged Component
     * @throws Exception it throws when the Component is exist already
     */
    public static Component merge(Component comp1, Component comp2) throws MergerException {
        compareChilds(comp1, comp2);
        isFind = false;
        return comp1;
    }

    /**
     * Method merges accepted Components
     * @param component root Component
     * @param comp2 child Component
     * @throws Exception it thrown when the Component is exist already
     */
    private static void compareChilds(Component component, Component comp2) throws MergerException {
        if (comp2.getClass().equals(File.class)) {
            if ((component.getClass().equals(File.class)) & (comp2.getName().equals(component.getName())) & (((File)comp2).getExtension().equals(((File)component).getExtension()))) {
                isFind = true;  //if our Folder contains the same File thrown Exception
               throw new MergerException();
            } else {
                component.add(comp2); //else we added the file
            }
        }

        for (Component child : ((Folder) comp2).getCompList()) {

            for (Component biggerChild : ((Folder) component).getCompList()) {

                if (biggerChild.getName().equals(child.getName())) {
                    compareChilds(biggerChild, child);         //if we find the same component, the method is
                } else {                                      // called recursively for the matched pair
                    continue;
                }
            }

            if (isFind == false) {
                for (Component inList : ((Folder) component).getCompList()) {  //check if there is no Component we want to add in the list
                    if (child.getName().equals(inList.getName())) {
                        isFind = true;
                        throw new MergerException();
                    }
                }
                 ((Folder) component).getCompList().add(child);              //added a new Component to the Folder
                 isFind = true;
                 break;
            }
        }
    }
}


