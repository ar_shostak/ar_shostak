package mog.epam.java_course.home_work11;

import java.io.*;
import java.util.ArrayList;

/**
 * The class is intended for serialization and deserialization of the created file system
 */
public class Serializer {

    /**
     * Used for serialization created object on HDD.
     * @param component component for serialization
     */
    public void serialize(Component component) {
        try (ObjectOutputStream objOut = new ObjectOutputStream(new FileOutputStream("Component.txt"))) {
            objOut.writeObject(component);
        } catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Used for loading a previously saved file system from disk.
     * @return previously saved file system
     */
    public Component deSerialize() {
        Component component = new Folder("root", new ArrayList<Component>());
        try (ObjectInputStream objOut = new ObjectInputStream(new FileInputStream("Component.txt"))) {
            component = (Component)objOut.readObject();
        } catch(ClassNotFoundException r) {
            r.printStackTrace();
        } catch(IOException e) {
            e.printStackTrace();
        }
        return component;
    }
}
