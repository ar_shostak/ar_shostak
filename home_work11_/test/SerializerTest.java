package mog.epam.java_course.home_work11;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class SerializerTest {
    private String path1;
    private String path2;
    private Component actual1;
    private Component actual2;

    @Before
    public void init() {
        path1 = "root/folder1/java.txt";
        path2 = "root/folder1/java22.txt";
        actual1 = new ComponentBuilder().build(path1.split("/"));
        actual2 = new ComponentBuilder().build(path2.split("/"));
    }

    @After
    public void clear() {
        path1 = null;
        path2 = null;
        actual1 = null;
        actual2 = null;
    }

    @Test
    public void testSerializeDeserialize() throws MergerException {
        Component expected = new Folder("root", new ArrayList<Component>());
        Component innerFolder = new Folder("folder1", new ArrayList<Component>());
        innerFolder.add(new File("java", "txt"));
        innerFolder.add(new File("java22", "txt"));
        expected.add(innerFolder);
        Component testComponent = Merger.merge(actual1, actual2);
        new Serializer().serialize(testComponent);
        String path3 = "root/folder3/java.txt";
        testComponent = Merger.merge(testComponent, new ComponentBuilder().build(path3.split("/")));
        testComponent = new Serializer().deSerialize();
        assertEquals(expected, testComponent);
    }
}
