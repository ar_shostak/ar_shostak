package mog.epam.java_course.home_work11;

import mog.epam.java_course.home_work05.Parser;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    @Test
    public void testValidate() {
        boolean expected = true;
        boolean actual = Parser.validate("root/folder1/java.txt");
        assertEquals(expected, actual);
    }

    @Test
    public void testValidateNegative() {
        boolean expected = false;
        boolean actual = Parser.validate("root/folder1/rt.er.er.");
        assertEquals(expected, actual);
    }
}
