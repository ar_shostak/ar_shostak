package app.service.impl;

import app.dao.DAOProvider;
import app.entity.Good;
import java.sql.SQLException;
import java.util.List;

public class GoodsServiceImpl {

    /**
     * @return list contains all Goods
     * @throws SQLException an exception that provides information on a database access
     * error or other errors.
     */
    public List<Good> getGoods() throws SQLException {
        return DAOProvider.getInstance().getGoodDao().getAllGoods();
    }
}
