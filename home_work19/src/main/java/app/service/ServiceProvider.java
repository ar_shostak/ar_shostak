package app.service;

import app.service.impl.GoodsServiceImpl;
import app.service.impl.OrderGoodsServiceImpl;
import app.service.impl.OrderService;
import app.service.impl.UserService;
import org.springframework.context.ApplicationContext;

public class ServiceProvider {
    static ApplicationContext context = ContextUtil.getContext();


    public static ServiceProvider instance = (ServiceProvider) context.getBean("serviceProvider");

    /**
     * Return the single provider object
     * @return the {@link ServiceProvider}
     */
    public static ServiceProvider getInstance() {
        return instance;
    }

    public static UserService userService = (UserService)context.getBean("userService");
    public static OrderService orderService = (OrderService)context.getBean("orderService");
    public static OrderGoodsServiceImpl orderGoodsService = (OrderGoodsServiceImpl)context.getBean("orderGoodsService");
    public static GoodsServiceImpl goodsService = (GoodsServiceImpl)context.getBean("goodsService");

    /**
     * @return the UserService object
     */
    public UserService getUserService(){
        return userService;
    }

    /**
     * @return the OrderService object
     */
    public OrderService getOrderService(){
        return orderService;
    }

    /**
     * @return the OrderGoodsService object
     */
    public OrderGoodsServiceImpl getOrderGoodsService(){
        return orderGoodsService;
    }

    /**
     * @return the GoodsService object
     */
    public GoodsServiceImpl getGoodsService(){
        return goodsService;
    }
}
