package app.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
    private static final String RETURN_DB_URL = "jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1";
    Connection connection;

    /**
     * Create instance of ConnectionProvider that contains connection instance
     */
    public ConnectionProvider() {
        try {
            connection = DriverManager.getConnection(RETURN_DB_URL);
        } catch (SQLException e) {
            throw new RuntimeException("Unable get base", e);
        }
    }

    /**
     * @return created connection
     */
    public Connection getConnection() {
        return connection;
    }
}
