package app.dao;

import app.connection.ConnectionProvider;
import app.dao.impl.GoodDaoImpl;
import app.dao.impl.OrderDaoImpl;
import app.dao.impl.OrderGoodsDaoImpl;
import app.dao.impl.UserDaoImpl;
import app.service.ContextUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOProvider {
    private static final String CREATE_DB_URL = "jdbc:h2:mem:testdb;INIT=RUNSCRIPT FROM 'classpath:create_tables.sql'\\;RUNSCRIPT FROM 'classpath:populate.sql';DB_CLOSE_DELAY=-1";

    public static final DAOProvider instance = (DAOProvider) ContextUtil.getContext().getBean("daoProvider");

    /**
     * Return the single provider object
     * @return the {@link DAOProvider}
     */
    public static DAOProvider getInstance() {
        return instance;
    }

    /**
     * The constructor establishes a connection and creates the provider object
     */
    private DAOProvider(){
       initBase();
    }

    /**
     * Creates a connection, creates a database, and initializes its original data.
     */
    private void initBase() {
        try {
            Connection connection = DriverManager.getConnection(CREATE_DB_URL);
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException("Unable init base", e);
        }
    }

    public static UserDao userDao = new UserDaoImpl((ConnectionProvider)ContextUtil.getContext().getBean("connectionProvider"));
    public static GoodDao goodDao = new GoodDaoImpl((ConnectionProvider)ContextUtil.getContext().getBean("connectionProvider"));
    public static OrderDao orderDao = new OrderDaoImpl((ConnectionProvider)ContextUtil.getContext().getBean("connectionProvider"));
    public static OrderGoodsDao orderGoods = new OrderGoodsDaoImpl((ConnectionProvider)ContextUtil.getContext().getBean("connectionProvider"));

    /**
     * @return the GoodDao object
     */
    public UserDao getUserDao(){
       return userDao;
    }

    /**
     * @return the GoodDao object
     */
    public GoodDao getGoodDao(){
        return goodDao;
    }

    /**
     * @return the OrderDao object
     */
    public OrderDao getOrderDao(){
        return orderDao;
    }

    /**
     * @return the OrderGoodsDao object
     */
    public OrderGoodsDao getOrderGoodsDao(){
        return orderGoods;
    }

}
