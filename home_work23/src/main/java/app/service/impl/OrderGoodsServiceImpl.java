package app.service.impl;

import app.dao.OrderDao;
import app.entity.Good;
import app.entity.Order;
import app.utils.GoodsUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import java.util.*;

@Service
@Lazy
public class OrderGoodsServiceImpl {
    public static final String PRINT_GOODS = "goodsList";
    public static final String PRICE = "price";

    private OrderDao orderDao;
    private GoodsServiceImpl goodService;
    private OrderServiceImpl orderService;

    /**
     * @param orderDao the {@link OrderDao}
     * @param orderService the {@link OrderServiceImpl}
     */
    public OrderGoodsServiceImpl(GoodsServiceImpl goodService, OrderServiceImpl orderService, OrderDao orderDao) {
        this.goodService = goodService;
        this.orderService = orderService;
        this.orderDao = orderDao;
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred id
     * @param orderId id of order for list building
     * @return the list
     */
    @Transactional(readOnly = true)
    public List<Good> getGoods(Long orderId){
        return orderService.get(orderId.toString()).getGoods();
    }

    /**
     * Returns the map contains name of item, its price and number of it in order
     * @param goods list of goods for map building
     * @return the map
     */
    @Transactional
    public Map<String, Integer> getOrderedGoods(List<Good> goods) {
        Map<String, Integer> map = new HashMap<>();
        for(Good good : goods) {
            String item = good.getName() + " (" + good.getPrice() + " $)";
            int value = 1;
            if (map.containsKey(item)) {
                value = map.get(item) + 1;
                map.remove(item);
            }
            map.put(item, value);
        }
        return map;
    }

    /**
     * Added product by name to the OrderGoods db table
     * @param name name of product to be added
     * @param orderId id of order in which the product included
     */
    @Transactional
    public boolean add(String name, Long orderId){
        Good good = (goodService.getGood(GoodsUtil.getName(name)));
        Order order = orderService.get(orderId.toString());
        order.addGood(good);
        orderDao.updateOrder(order);
        return true;
    }

    /**
     * Create map with parameters for print check
     * @param basket the map contains goods
     * @return the map with parameters
     */
    @ModelAttribute("goods")
    public List<String> createPresentationParams(@NotNull Map<String, Integer> basket){
        List<String>params = new ArrayList<>();
        int i = 1;
        for (Map.Entry<String, Integer> pair : basket.entrySet()) {
            params.add(i + ") "+ pair.getKey() + " x " + pair.getValue());
            i += 1;
        }
        return params;
    }

    /**
     * Counts total price of order
     * @param goods the list og goods in order
     * @return the price
     */
    @Transactional
    public String countPrice(List<Good> goods) {
        double orderPrice = GoodsUtil.countTotalPrice(goods);
        return String.format("%.1f", orderPrice);
    }
}
