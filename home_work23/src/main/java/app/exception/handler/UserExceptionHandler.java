package app.exception.handler;

import static app.page_path.PagePath.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class UserExceptionHandler  {
    private static String ERROR = "error";

    @ExceptionHandler(NoHandlerFoundException.class)
    public ModelAndView handleError404(Exception e)   {
        return new ModelAndView(ERROR_PATH_404.getURL()).addObject(ERROR, e.getMessage());
    }
}
