package app.controller;

import app.entity.Order;
import app.entity.User;
import app.service.ServiceException;
import app.service.impl.GoodsServiceImpl;
import app.service.impl.OrderGoodsServiceImpl;
import app.utils.GoodsUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static app.controller.InitializerController.ALL_GOODS;
import static app.page_path.PagePath.*;
import static app.service.impl.OrderGoodsServiceImpl.PRICE;
import static app.service.impl.OrderGoodsServiceImpl.PRINT_GOODS;

@Controller
@RequestMapping({"/"})
public class GoodsAddController{
    private static final String ITEM = "good";
    private static final String EMPTY_ELEMENT = "--Choose item--";
    public static final String BASKET = "order";
    public static final String NAME = "name";
    private static final String ADD_GOOD_URL = "/goodsAddServlet";
    private static final String COMPLETE_ORDER_URL = "/complete";
    private static final String CURRENT_USER = "currentUser";

    private OrderGoodsServiceImpl service;
    private GoodsServiceImpl goodsService;

    public GoodsAddController(OrderGoodsServiceImpl service, GoodsServiceImpl goodsService) {
        this.goodsService = goodsService;
        this.service = service;
    }

    /**
     * Handles {@link HttpServlet} POST Method.
     * If the item has not been selected returns to the selection page. If selected - adds it to
     * the basket and returns to the selection page
     * @param request  the {@link HttpServletRequest}
     * @param item the selected item
     */
    @RequestMapping(value = ADD_GOOD_URL, method = RequestMethod.POST)
    protected ModelAndView addGood(final HttpServletRequest request, @RequestParam(ITEM) String item) {
        HttpSession session = request.getSession();
        putToBasket(item, session);
        return createView(session, ADD.getURL());
    }

    /**
     * @param session the {@link HttpSession}
     * @param path the path to the resource
     * @return the ModelAndView contains data with chosen items
     */
    private ModelAndView createView(HttpSession session, String path) {
        ModelAndView model = new ModelAndView(path);
        model.addAllObjects(Stream.of(new Object[][]{
                {NAME, ((User)session.getAttribute(CURRENT_USER)).getLogin()},
                {ALL_GOODS, session.getAttribute(ALL_GOODS)},
                {BASKET, session.getAttribute(BASKET)},
        }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])));
        return model;
    }

    /**
     * Handles {@link HttpServlet} GET Method. Redirect user if no goods were chosen after authentication
     * and order submitting
     */
    @RequestMapping(value = {ADD_GOOD_URL, COMPLETE_ORDER_URL}, method = RequestMethod.GET)
    public ModelAndView doGet() {
        return new ModelAndView(ADD.getURL());
    }

    /**
     * Add not empty goods to basket
     * @param chosenItem item for adding
     * @param session the {@link HttpSession}
     */
    public void putToBasket(final String chosenItem, HttpSession session) {
        Order order = ((User)session.getAttribute(CURRENT_USER)).getOrder();
        if (!chosenItem.equals(EMPTY_ELEMENT)) {
            order.getGoods().add((goodsService.getGood(GoodsUtil.getName(chosenItem))));
        }
        session.setAttribute(BASKET, service.getOrderedGoods(order.getGoods()));
    }

    /**
     * @param request the {@link HttpServletRequest}
     * @param item the chosen item
     * @return the ModelAndView contains completed order
     */
    @RequestMapping(value = COMPLETE_ORDER_URL, method = RequestMethod.POST)
    protected ModelAndView completeOrder(final HttpServletRequest request, @RequestParam(ITEM) String item) throws ServiceException {
        HttpSession session = request.getSession();
        putToBasket(item, session);
        return printCheckView(session);
    }

    /**
     * Chooses path for redirection by transferred basket's content
     * @param session {@link HttpSession} current session
     * @return path to redirect
     */
    private ModelAndView printCheckView(HttpSession session) {
        ModelAndView model;
        User user = (User)session.getAttribute(CURRENT_USER);
        Map<String, Integer> basket = (Map<String, Integer>)session.getAttribute(BASKET);
        if (basket.size() == 0) {
            model = new ModelAndView(EMPTY_BASKET_ERROR.getURL());

        } else {
            model = new ModelAndView(PRINT_CHECK.getURL()).addObject(PRINT_GOODS, service.createPresentationParams(basket));
            model.addAllObjects(Stream.of(new Object[][]{
                    {NAME, user.getLogin()},
                    {PRICE, service.countPrice(user.getOrder().getGoods())},
            }).collect(Collectors.toMap(data -> (String) data[0], data -> data[1])));
        }
        return model;
    }
}


