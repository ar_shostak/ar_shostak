package app.dao;

import app.entity.Order;

import java.util.Optional;

public interface OrderDao {

    /**
     * Returns the order by the transferred user's id
     * @param id user's id
     * @return the {@link Order}
     */
    Optional<Order> getOrderByUserId(Long id);

    /**
     * Add order to Order table
     * @param order the order
     */
    void addToOrder(Order order);

    /**
     * Update user's Order with transferred price
     * @param totalPrice price to be assigned to the order
     * @param userId id of user to update
     */
    boolean updateOrderById(double totalPrice, Long userId);

    /**
     * Update user's Order with transferred price
     * @param order the order
     */
    boolean updateOrder(Order order);
}
