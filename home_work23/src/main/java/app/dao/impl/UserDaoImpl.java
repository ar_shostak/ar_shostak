package app.dao.impl;

import app.dao.UserDao;
import app.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
@Lazy
public class UserDaoImpl implements UserDao {
    private static final String NAME_COLUMN = "userName";
    private static final String SELECT_USER = "from User where login = :userName";
    private static final String SELECT_ALL_USERS = "from User";

    private SessionFactory factory;

    public UserDaoImpl(SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Returns the user by the transferred name
     * @param userName user's name
     * @return the {@link User}
     * error or other errors.
     */
    @Override
    public Optional<User> getUserByName(String userName) {
        Session session = factory.getCurrentSession();
        return session.createQuery(SELECT_USER).setParameter(NAME_COLUMN, userName).uniqueResultOptional();
    }

    /**
     * Return list contains all users in base
     * @return the list with all users
     * error or other errors.
     */
    @Override
    public List<User> getAllUsers(){
        Session session = factory.getCurrentSession();
        return session.createQuery(SELECT_ALL_USERS).getResultList();
    }

    /**
     * Add the user to the base
     * @param user the user
     */
    @Override
    public void addUser(User user){
        Session session = factory.getCurrentSession();
        session.save(user);
    }
}
