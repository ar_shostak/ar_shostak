package app.dao.impl;

import app.dao.GoodDao;
import app.entity.Good;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
@Lazy
public class GoodDaoImpl implements GoodDao {
    private static final String NAME_COLUMN = "name";
    private static final String SELECT_GOOD_SQL_STATEMENT = "from Good where name = :name";
    private static final String SELECT_ALL_GOODS = "from Good";

    private SessionFactory factory;

    public GoodDaoImpl(SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Returns the good by the transferred name
     * @param name good's name
     * @return the {@link Good}
     */
    @Override
    public Optional<Good> getGood(String name) {
        Session session = factory.getCurrentSession();
        return (session.createQuery(SELECT_GOOD_SQL_STATEMENT).setParameter(NAME_COLUMN, name).uniqueResultOptional());
    }

    /**
     * Returns the good by the transferred id
     * @param goodId good's id
     * @return the {@link Good}
     */
    @Override
    public Optional<Good> getGood(Long goodId) {
        Session session = factory.getCurrentSession();
        return Optional.ofNullable(session.get(Good.class, goodId));
    }

    /**
     * Return list contains all Goods in base
     * @return the list
     */
    @Override
    public List<Good> getAllGoods() {
        Session session = factory.getCurrentSession();
        return session.createQuery(SELECT_ALL_GOODS).getResultList();
    }
}
