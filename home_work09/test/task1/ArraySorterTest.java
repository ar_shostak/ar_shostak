package mog.epam.java_course.home_work09.task01;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class ArraySorterTest {
   @Test
    public void testSortByNumberSum() {
       int[] array = new int[]{-4, 16, 10, 8, 12};
       int[] expected = new int[]{10, 12, -4, 16, 8};
       int[] actual = ArraySorter.sortByNumberSum(array);
       assertArrayEquals(expected, actual);
   }

    @Test
    public void testSortByNumberSumDuplicate() {
        int[] array = new int[]{237, 237, -237, -4, 4, 12, -12};
        int[] expected = new int[]{-12, 12, -4, 4, -237, 237, 237};
        int[] actual = ArraySorter.sortByNumberSum(array);
        assertArrayEquals(expected, actual);
    }
}
