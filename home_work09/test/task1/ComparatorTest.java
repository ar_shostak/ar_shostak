package mog.epam.java_course.home_work09.task01;

import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorTest {
    Comparator comparator = new ArraySorterComparator();
    @Test
    public void testCompare() {
        int expected = 1;
        int actual = comparator.compare(23, 101);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareMinus() {
        int expected = - 1;
        int actual = comparator.compare(111, 23);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testCompareSame() {
        int expected = 0;
        int actual = comparator.compare(23, 23);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testCompareNull() {
        int actual = comparator.compare(23, null);
    }

}
