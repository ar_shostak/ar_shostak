package mog.epam.java_course.home_work09.task02;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.assertEquals;

public class MathOperatorTest {
    Set set1;
    Set set2;

    @Before
    public void initialSet() {
        set1 = new HashSet<>();
        set1.add("A");
        set1.add("B");
        set1.add("C");

        set2 = new HashSet<>();
        set2.add("A");
        set2.add("B");
        set2.add("D");
        set2.add("F");
    }

    @After
    public void clear() {
        set1 = null;
        set2 = null;
    }

    @Test
    public void testUnion() {
        MathOperator<String> oper = new MathOperator<>();
        Set actual = oper.union(set1, set2);
        Set expected = new HashSet(Arrays.asList(new String[] {"A", "B", "C", "D", "F"}));
        assertEquals(expected, actual);
    }

    @Test
    public void testIntersection() {
        MathOperator<String> oper = new MathOperator<>();
        Set actual = oper.intersection(set1, set2);
        Set expected = new HashSet(Arrays.asList(new String[] {"A", "B"}));
        assertEquals(expected, actual);
    }

    @Test
    public void testMius() {
        MathOperator<String> oper = new MathOperator<>();
        Set actual = oper.minus(set1, set2);
        Set expected = new HashSet(Arrays.asList(new String[] {"C"}));
        assertEquals(expected, actual);
    }

    @Test
    public void testDifference() {
        MathOperator<String> oper = new MathOperator<>();
        Set actual = oper.difference(set1, set2);
        Set expected = new HashSet(Arrays.asList(new String[] {"C", "D", "F"}));
        assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void testNullArgument() {
        MathOperator<String> oper = new MathOperator<>();
        Set newSet = null;
        Set actual = oper.difference(set1, newSet);
    }
}
