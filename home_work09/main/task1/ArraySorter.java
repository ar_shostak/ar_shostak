package mog.epam.java_course.home_work09.task01;

import java.util.ArrayList;
import java.util.List;

public class ArraySorter {

    /**
     * Sorts passed array by the sum of the digits of its number in ascending order
     * @param array contains integer numbers for sorting
     * @return sorted array
     */
    public static int[] sortByNumberSum(int[] array) {
        List<Integer> list = new ArrayList<Integer>();
        int i = 0;
        while (i < array.length) {
            list.add(array[i]);
            i++;
        }
        list.sort(new ArraySorterComparator());
        array = list.stream().mapToInt(j->j).toArray();
        return array;
    }
}
