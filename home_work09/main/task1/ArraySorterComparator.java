package mog.epam.java_course.home_work09.task01;

import java.util.Comparator;

public class ArraySorterComparator implements Comparator<Integer> {
    /**
     * Compares its two arguments for order.  Returns a negative integer if the first
     * is less then the second, a positive integer if the first argument is greater , or null if
     * equal. For comparison it uses the sum of the digits of the passed number
     * this fact.
     * @param o1 the first object to be compared.
     * @param o2 the second object to be compared.
     * @return a negative integer, zero, or a positive integer as the
     * first argument is less than, equal to, or greater than the
     * second.
     */
    @Override
    public int compare(Integer o1, Integer o2){
        if(o1 == null || o2 == null) {
            throw new NullPointerException();
        }
            if(findSum((Integer)o1) > (findSum((Integer)o2))) {
                return 1;
            } else if(findSum((Integer)o2) > (findSum((Integer)o1))) {
                return -1;
            } else if((findSum((Integer)o1) == (findSum((Integer)o2))) & ((Integer)o1 < 0)) {
                return -1;
            } else if((findSum((Integer)o1) == (findSum((Integer)o2))) & ((Integer)o2 <  0)) {
                return 1;
            } else return 0;
        }

    /**
     * Finds the sum of the digits of the passed number
     * @param i number for operation
     * @return he sum of the digits of the passed number
     */
    private static int findSum(Integer i) {
        if (i < 0) {
            i = 0 - i;
        }
        int sum = 0;
        for(; i > 0; i = i / 10) {
            sum = sum + i % 10;
        }
        return sum;
    }

}
