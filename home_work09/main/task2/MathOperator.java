package mog.epam.java_course.home_work09.task02;

import java.util.Iterator;
import java.util.Set;

/**
 * Designed to perform mathematical operations on sets. Provides union, intersection, difference,
 * exclusion. Parameterized for compile-time type security
 */
public class MathOperator<E> {

    /**
     * Merges sets. Returns a set containing non-repeating elements from both
     * @param set1 first set
     * @param set2 second set
     * @return the result set
     */
    public Set<E> union(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        set1.addAll(set2);
        return set1;
    }

    /**
     * Finds an intersection of two sets. Returns the total set containing
     * elements repeating in both sets
     * @param set1 first set
     * @param set2 second set
     * @return the result set
     */
    public Set<E> intersection(Set<E> set1, Set<E> set2) {
       checkForNull(set1, set2);
       Iterator itr = set1.iterator();
       while(itr.hasNext()) {
          Object obj = itr.next();
          if (!(set2.contains(obj))) {
              itr.remove();
          }
       }
       return set1;
    }

    /**
     * Produces the set difference of two sets. Subtracts from the first set elements, contains in
     * the last set.
     * @param set1 diminished set
     * @param set2 deductible set
     * @return subset contains esult of operation
     */
    public Set<E> minus(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        Iterator itr = set1.iterator();
        while(itr.hasNext()) {
            Object obj = itr.next();
            if (set2.contains(obj)) {
                itr.remove();
            }
        }
        return set1;
    }

    /**
     * Returns a set consisting of elements, that are not found in both sets
     * @param set1 first set
     * @param set2 second set
     * @return the result set
     */
    public Set<E> difference(Set<E> set1, Set<E> set2) {
        checkForNull(set1, set2);
        Iterator itr = set1.iterator();
        while(itr.hasNext()) {
            Object obj = itr.next();
            if (set2.contains(obj)) {
                itr.remove();
                set2.remove(obj);
            }
        }
        set2.addAll(set1);
        return set2;
    }

    /**
     * Checks if both arguments not null. If one of them is null throws NullPointerException
     * @param set1 argument to check
     * @param set2 argument to check
     */
    private void checkForNull(Set<E> set1, Set<E> set2) {
        if ((set1 == null) || (set2 == null)) {
            throw new NullPointerException();
        }
    }

}
