package app.config.security;

import app.dao.impl.UserDaoImpl;
import app.entity.User;
import app.exception.UserNameNotFoundException;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import javax.inject.Inject;

@Service
@Lazy
public class CustomUserDetailsService implements UserDetailsService {

    private UserDaoImpl dao;

    @Inject
    public void setDao(UserDaoImpl dao){
        this.dao=dao;
    }

    /**
     * Load user from DB if it is exist. Else throw UserNameNotFoundException
     * @param name user's name
     * @return the {@link UserDetails}
     * @throws UserNameNotFoundException throws when no one user found in data base
     */
    @Override
     public UserDetails loadUserByUsername(final String name) throws UserNameNotFoundException {
        final User user =dao.getUserByName(name)
                        .orElseThrow(() -> new UsernameNotFoundException("User with email='" + name + "' not found"));
        return new CustomUserPrincipal(user);
    }
}
