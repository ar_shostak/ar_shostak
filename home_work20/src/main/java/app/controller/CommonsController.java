package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import static app.constants.Constants.LOGIN;
import static app.constants.Constants.LOGOUT;
import static app.page_path.PagePath.HOME_PAGE;
import static app.page_path.PagePath.LOGOUT_PAGE;

@Controller
@RequestMapping()
public class CommonsController {

  @GetMapping(LOGIN)
  public String login(ModelAndView model) {
    return HOME_PAGE.getPath();
  }

  @GetMapping(LOGOUT)
  public String logout() {
    return LOGOUT_PAGE.getPath();
  }
}
