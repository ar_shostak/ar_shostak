package app.service;

import app.dao.UserDto;
import app.dao.impl.UserDaoImpl;
import app.entity.Order;
import app.entity.User;
import app.service.impl.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import java.util.Optional;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @Mock
    private UserDaoImpl daoMock;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void testGetUser() {
        //Given
        User user = new User(1, "Archi", "112", "ADMIN", new Order());
        UserDto expected = new UserDto(3, "David", "111", "ADMIN", new Order());
        when(daoMock.getUserById(1L)).thenReturn(Optional.of(user));
        //When
        UserDto actual = userService.getUser(1L);
        //Then
        assertEquals(expected, actual);
    }

    @Test(expected = org.hibernate.service.spi.ServiceException.class)
    public void testGetUserUnsuccessful() {
        //Given
        when(daoMock.getUserById(1L)).thenReturn(Optional.empty());
        //When
        UserDto actual = userService.getUser(1L);
    }

    @Test
    public void testCreate() {
        //Given
        User user = new User(3, "David", "111", "ADMIN", new Order());
        UserDto expected = new UserDto(3, "David", "111", "ADMIN", new Order());
        when(daoMock.getUserByName("David")).thenReturn(Optional.of(user));
        //When
        UserDto actual = userService.getUser(3L);
        //Then
        assertEquals(expected, actual);
    }

    @Test(expected = org.hibernate.service.spi.ServiceException.class)
    public void testUnsuccessfulCreate() {
        //Given
        when(daoMock.getUserByName("Dav")).thenReturn(Optional.empty());
        //When
        UserDto actual = userService.getUser(3L);
    }
}