INSERT INTO Users VALUES (1,'sa','$2a$04$sWQcerEz6hUZbNxc0qjxOOUnyy1AF8A5a8vWlKJJGwyzG2w7q0p5a', 'USER');
INSERT INTO Users VALUES (2,'Surfer','$2a$04$sWQcerEz6hUZbNxc0qjxOOUnyy1AF8A5a8vWlKJJGwyzG2w7q0p5a', 'SURFER');
INSERT INTO Users VALUES (3,'Archi','$2a$04$sWQcerEz6hUZbNxc0qjxOOUnyy1AF8A5a8vWlKJJGwyzG2w7q0p5a', 'ADMIN');
INSERT INTO Users VALUES (4,'David','$2a$04$sWQcerEz6hUZbNxc0qjxOOUnyy1AF8A5a8vWlKJJGwyzG2w7q0p5a', 'ADMIN');
INSERT INTO Goods VALUES (1, 'SQL for beginners', 3.2);
INSERT INTO Goods VALUES (2, 'Advanced SQL', 5.2);
INSERT INTO Goods VALUES (3, 'Head First', 5);
INSERT INTO Goods VALUES (4, 'Mazda6 tutorial', 4);