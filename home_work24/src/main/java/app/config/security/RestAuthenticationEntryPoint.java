package app.config.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

    /**
     *  Commences an authentication scheme
     * @param request the {@link HttpServletRequest}
     * @param response the {@link HttpServletResponse}
     * @param authException the {@link AuthenticationException}
     * @throws IOException thrown when input/output exception occurs
     */
    @Override
    public void commence(HttpServletRequest request,
            HttpServletResponse response,
            AuthenticationException authException) throws IOException {

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
                "Unauthorized" + request.getParameter("Username"));
    }
}
