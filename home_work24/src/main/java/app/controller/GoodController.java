package app.controller;

import app.entity.Good;
import app.exception.ServiceException;
import app.service.impl.GoodsServiceImpl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import java.util.List;

@RestController
@RequestMapping("/app/goods")
public class GoodController {

    private GoodsServiceImpl service;

    @Inject
    public GoodController(GoodsServiceImpl service) {
        this.service = service;
    }

    /**
     * Returns the list of all users
     * @return {@link ResponseEntity}
     */
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Good>> getGoods(){
        return ResponseEntity.ok(service.getGoods());
    }

    /**
     * Returns the good by its id.
     *  @param id - the id of the good
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Good> getGood(@NotBlank(message = "The id cannot be blank") @PathVariable(value = "id") final long id) throws ServiceException {
        Good good = service.getGood(id);
        return ResponseEntity.ok(good);
    }
}