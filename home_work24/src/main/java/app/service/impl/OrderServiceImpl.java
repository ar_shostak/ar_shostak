package app.service.impl;

import app.dao.OrderDao;
import app.entity.Order;
import app.exception.ServiceException;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import java.util.Optional;

@Service
@Lazy
public class OrderServiceImpl {
    private OrderDao orderDao;

    @Inject
    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    /**
     * Return the Order by User's id. If it not exist, create a new order and the return
     * @param userId user's id
     * @return the Order
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Order get(Long userId) {
        Optional<Order> order = orderDao.getOrderByUserId(userId);
        return order.orElseGet(() -> create(userId));
    }

    /**
     * Create a new Order by the transferred user's id
     * @param id the user's id
     * @return created user
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Order create(Long id){
        orderDao.createOrder(id);
        return orderDao.getOrderByUserId(id).orElseThrow(() -> new ServiceException("Order can't be created"));
    }

    /**
     * Clears basket when the order is finished
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateById(double orderPrice, Long orderId) {
        return orderDao.updateOrderById(orderPrice, orderId);
    }
}