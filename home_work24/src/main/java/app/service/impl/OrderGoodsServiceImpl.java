package app.service.impl;

import app.dao.OrderGoodsDao;
import app.dao.impl.OrderGoodsDaoImpl;
import app.entity.Good;
import app.entity.OrderGoods;
import app.utils.GoodsUtil;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Lazy
public class OrderGoodsServiceImpl {
    private OrderGoodsDao orderGoodsDao;
    private GoodsServiceImpl goodService;
    private OrderServiceImpl orderService;

    /**
     * @param orderGoodsDao the {@link OrderGoodsDaoImpl}
     * @param goodService the {@link OrderGoodsDaoImpl}
     */
    @Inject
    public void setDao(OrderGoodsDao orderGoodsDao, GoodsServiceImpl goodService, OrderServiceImpl orderService) {
        this.orderGoodsDao = orderGoodsDao;
        this.goodService = goodService;
        this.orderService = orderService;
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred id
     * @param orderId id of order for list building
     * @return the list
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Good> getGoods(Long orderId){
        List<OrderGoods> inCurrentOrder = orderGoodsDao.getByOrderId(orderId);
        return getGoodsInCurrentOrder(inCurrentOrder);
    }

    /**
     * Returns the map contains name of item, its price and number of it in order
     * @param orderId id of order for map building
     * @return the map
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public Map<String, Integer> getOrderedGoods(Long orderId) {
        Map<String, Integer> map = new HashMap<>();
        for(Good good : getGoods(orderId)) {
            String item = good.getName() + " (" + good.getPrice() + " $)";
            int value = 1;
            if (map.containsKey(item)) {
                value = map.get(item) + 1;
                map.remove(item);
            }
            map.put(item, value);
        }
        return map;
    }

    /**
     * Added product by name to the OrderGoods db table
     * @param good product to be added
     * @param orderId id of order in which the product included
     */
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean add(Good good, Long orderId){
        orderGoodsDao.addToOrderGood(orderId, good.getId());
        orderService.updateById(countPrice(orderId), orderId);
        return true;
    }

    /**
     * Returns the list contains goods in order. Sampling by transferred OrderGoods list
     * @param goodsInCurrentOrder the OrderGoods list for good list creation
     * @return the list
     */
    protected List<Good> getGoodsInCurrentOrder(List<OrderGoods> goodsInCurrentOrder){
        List<Good> orderedGoods = new ArrayList<>();
        for (OrderGoods current : goodsInCurrentOrder) {
            orderedGoods.add(goodService.getGood(current.getGoodId()));
        }
        return orderedGoods;
    }

    /**
     * Counts total price of order by id
     * @param orderId the order id
     * @return the price
     */
    public double countPrice(long orderId) {
        return GoodsUtil.countTotalPrice(getGoods(orderId));
    }
}
