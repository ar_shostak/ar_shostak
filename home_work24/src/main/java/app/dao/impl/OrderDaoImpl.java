package app.dao.impl;

import app.dao.OrderDao;
import app.entity.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import java.util.Optional;

@Repository
@Lazy
public class OrderDaoImpl implements OrderDao {
    private static final String SELECT_ORDER_BY_USER = "from Order where userId = :userId";
    private static final String SELECT_ORDER_BY_ID = "from Order where id = :id";
    private static final String USER_ID = "userId";
    private static final String ID = "id";

    private SessionFactory factory;

    @Inject
    public OrderDaoImpl(SessionFactory factory) {
        this.factory = factory;
    }

    /**
     * Returns the order by the transferred user's id
     * @param userId user's id
     * @return the {@link Order}
     */
    @Override
    public Optional<Order> getOrderByUserId(Long userId){
        Optional<Order> order;
        try (Session session = factory.openSession()) {
            order = Optional.ofNullable((Order)session.createQuery(SELECT_ORDER_BY_USER).setParameter(USER_ID, userId).uniqueResult());
        }
        return order;
    }

    /**
     * Returns the order by the transferred order's id
     * @param orderId order's id
     * @return the {@link Order}
     */
    public Optional<Order> getOrderByOrderId(Long orderId){
        Optional<Order> order;
        try (Session session = factory.openSession()) {
            order = Optional.ofNullable((Order)session.createQuery(SELECT_ORDER_BY_ID).setParameter(ID, orderId).uniqueResult());
        }
        return order;
    }

    /**
     * Add to Order table by the transferred user's id
     * @param userId id user's id
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void createOrder(Long userId) {
        try(Session session = factory.openSession()) {
            Order order = new Order();
            order.setUserId(userId);
            order.setTotalPrice(0.0);
            session.save(order);
        }
    }

    /**
     * Update user's Order with transferred price
     * @param totalPrice price to be assigned to the order
     * @param orderId id of the order to update
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean updateOrderById(double totalPrice, long orderId) {
        try (Session session = factory.openSession()) {
            Order order = getOrderByOrderId(orderId).orElseThrow(() -> new RuntimeException("Order not found"));
            order.setTotalPrice(totalPrice);
            session.save(order);
            return true;
        }
    }
}
