package app.exception;

public class ServiceException extends RuntimeException {
    public ServiceException(String str){
        super(str);
    }
}
