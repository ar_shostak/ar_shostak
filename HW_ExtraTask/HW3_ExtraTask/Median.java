package mog.epam.java_course.HW3_ExtraTask;

import java.util.Random;

public final class Median {
    private static boolean left = true;

    private Median() {};

    /**
     * Method median find middle value in a sorted list of numbers. Acceps
     * {@code int} data
     * @param args an array contains the source data of {@code int} type
     * @return the value of median in type of {@code float}
     */
    public static float median(int[] args) {
        float result;
        int[] rez = args;
        int ind = args.length / 2;
        if (args.length % 2 == 0) {
            result = 0.5f * (findElement(args, ind) + findElement(args, ind - 1));
        } else {
            result = findElement(args, ind);
        }
        return result;
    }

    /**
     * Helper method to find element in an array by index without sort. The element is the same
     * as if the array were sorted
     * {@code int} data
     * @param rez an array contains the source data of {@code int} type
     * @param index index of element in sorted array
     * @return the value of element in type of {@code float}
     */
    private static float findElement(int[] rez, int index) {
        while(rez.length > 1) {
            if (isTheSame(rez) == true) {
                return rez[0];
            }
            int rezSize = rez.length;
            rez = quickSelect(rez, index);
            if (left == false) {
                int minus = rezSize - rez.length;
                index = index - minus;
                left = true;
            }
        }
        return (float)(rez[0]);
    }

    /**
     * Helper method to check if the array consists of the same values
     * @param args checked array consists of elements of {@code int} type
     */
    private static boolean isTheSame(int[] args) {
        int first = args[0];
        for(int i = 1; i < args.length; i++) {
            if(first == args[i]) {
                return true;
            }
        }
        return false;
    }

    /**
     * Helper method to find subarray containing desired value
     * @param arr an array contains the source data of {@code int} type
     * @param index index of element should be find
     * @return subarray containing desired value
     */
    private static int[] quickSelect(int[] arr, int index) {
        Random ran = new Random();
        int r = ran.nextInt(arr.length);
        int fundamental = arr[r];
        int counter = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] <= fundamental) {
                counter++;
            }
        }
        int[] subarr;

        if(counter >= index + 1) {
            subarr = fillTheArray(arr, counter, fundamental, true);
        } else {
            subarr = fillTheArray(arr, counter, fundamental, false);
            left = false;
        }
        return subarr;
    }
    /**
     * Helper method to find subarray containing desired value
     * @param array an array contains the source data of {@code int} type
     * @param newSize size of a new array containing the element should be find
     * @param fundamental the random value from array that divide the array on two parts
     * @param isLeft whether the search value is on the left side
     * @return subarray containing desired value
     */
    private static int[] fillTheArray(int[] array, int newSize, int fundamental, boolean isLeft) {
        int[] descreast;
        int counter = 0;
        if (isLeft == true) {
            descreast = new int[newSize];
            for (int i = 0; i < array.length; i++) {
                if (array[i] <= fundamental) {
                    descreast[counter] = array[i];
                    counter++;
                }
            }
        } else {
            descreast = new int[array.length - newSize];
            for (int i = 0; i < array.length; i++) {
                if (array[i] > fundamental) {
                    descreast[counter] = array[i];
                    counter++;
                }
            }
        }
        return descreast;
    }

    /**
     * Method median find middle value in a sorted list of numbers. Acceps
     * {@code int} data
     * @param args an array contains the source data of {@code int} type
     * @return the value of median in type of {@code double
     */
    public static double median(double[] args) {
        double result;
        double[] rez = args;
        int ind = args.length / 2;
        if (args.length % 2 == 0) {
            result = 0.5f * (findElement(args, ind) + findElement(args, ind - 1));
        } else {
            result = findElement(args, ind);
        }
        return result;

    }

    /**
     * Helper method to find element in an array by index without sort. The element is the same
     * as if the array were sorted
     * {@code double} data
     * @param rez an array contains the source data of {@code double} type
     * @param index index of element in sorted array
     * @return the value of element in type of {@code double}
     */
    private static double findElement(double[] rez, int index) {
        while(rez.length > 1) {
            if (isTheSame(rez) == true) {
                return rez[0];
            }
            int rezSize = rez.length;
            rez = quickSelect(rez, index);
            if (left == false) {
                int minus = rezSize - rez.length;
                index = index - minus;
                left = true;
            }
        }
        return (rez[0]);
    }

    /**
     * Helper method to check if the array consists of the same values
     * @param args checked array consists of elements of {@code double} type
     */
    private static boolean isTheSame(double[] args) {
        double first = args[0];
        for(int i = 1; i < args.length; i++) {
            if(first == args[i]) {
                return true;
            }
        }
        return false;
    }

     /** Helper method to find subarray containing desired value
     * @param arr an array contains the source data of {@code double} type
     * @param index index of element should be find
     * @return subarray containing desired value
     */
     private static double[] quickSelect(double[] arr, int index) {
         Random ran = new Random();
         int r = ran.nextInt(arr.length);
         double fundamental = arr[r];
         int counter = 0;
         for (int i = 0; i < arr.length; i++) {
             if (arr[i] <= fundamental) {
                 counter++;
             }
         }
         double[] subarr;

         if(counter >= index + 1) {
             subarr = fillTheArray(arr, counter, fundamental, true);
         } else {
             subarr = fillTheArray(arr, counter, fundamental, false);
             left = false;
         }
         return subarr;
     }

    /**
     * Helper method to find subarray containing desired value
     * @param array an array contains the source data of {@code double} type
     * @param newSize size of a new array containing the element should be find
     * @param fundamental the random value from array that divide the array on two parts
     * @param isLeft whether the search value is on the left side
     * @return subarray containing desired value
     */
    private static double[] fillTheArray(double[] array, int newSize, double fundamental, boolean isLeft) {
        double[] descreast;
        int counter = 0;
        if (isLeft == true) {
            descreast = new double[newSize];
            for (int i = 0; i < array.length; i++) {
                if (array[i] <= fundamental) {
                    descreast[counter] = array[i];
                    counter++;
                }
            }
        } else {
            descreast = new double[array.length - newSize];
            for (int i = 0; i < array.length; i++) {
                if (array[i] > fundamental) {
                    descreast[counter] = array[i];
                    counter++;
                }
            }
        }
        return descreast;
    }
}



