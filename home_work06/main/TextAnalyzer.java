package mog.epam.java_course.home_work06;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

/**
 * The program reads from the console the text and create the dictionary in alphabetical order
 * with a count of words repetitions. Allows non-repeating empty lines. To start analysis, press 
 * the spacebar three times
 */
public class TextAnalyzer {
    public static void main(String[] args) {
        String text = "";
        String line;
        String newline;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                if (!(line = reader.readLine()).equals("")) {
                    text = text + " " + line;
                } else if ((newline = reader.readLine()).equals("")) {
                        break;
                } else {
                    text = text + " " + newline;
                }
               }

        }catch (IOException e){
            System.out.println("IOException");
        }
        System.out.println("---The vocabulary---");
        String sortedText = TextLogic.removePunctuation(text);
        List<String> wordList = TextLogic.split(sortedText);
        Map<String, Integer> vocabulary = TextLogic.buildVocabulary(wordList);
        TextLogic.print(vocabulary);
    }

}
