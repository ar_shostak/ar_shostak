package mog.epam.java_course.home_work08.task1;

import java.util.AbstractList;

public class MyList<E> extends AbstractList<E> {

    /**
     * The maximum value of list's length.
     */
    private static final int MAX_SIZE = 20;

    /**
     * The default value of list's length.
     */
    private static final int DEFAULT_CAPACITY = 10;

    /**
     * The array in which the elements of the ArrayList are stored.
     */
    private Object[] element;

    /**
     * The number of elements this list contains.
     */
    private int size;

    /**
     * Constructs an empty list with the default initial capacity.
     */
    public MyList() {
        element = new Object[DEFAULT_CAPACITY];
        size = 0;
    }

    /**
     * Constructs an empty list with the specified initial capacity.
     * @param  initialSize  the initial capacity of the list
     * @throws IllegalArgumentException if the specified initial capacity
     * is negative or bigger then maximum capacity
     */
    public MyList(int initialSize) throws IllegalArgumentException {
        if ((initialSize < 0) || (initialSize > MAX_SIZE)) {
            throw new IllegalArgumentException("The size of MyList should be from 0 to 30");
        } else {
            element = new Object[initialSize];
            size = 0;
        }
    }

    /**
     * Returns the element at the specified position in this list.
     * @param  index index of the element to return
     * @return the element at the specified position
     * @throws IndexOutOfBoundsException
     */
    @Override
    public E get(int index) {
        if ((index > size) | (index < 0)) {
            throw new IndexOutOfBoundsException();
        } else return (E)element[index];
    }

    /**
     * Returns the number of elements in this list.
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }

     /** Replaces the element at the specified position in this list with
     * the specified element.
     * @param index index of the element to replace
     * @param object element to be stored instead of particular at that index position
     * @return the element that was at the specified position
     * @throws IndexOutOfBoundsException
     */
    @Override
    public Object set(int index, Object object) {
        Object toReturn;
        if ((index > size) | (index < 0)) {
            throw new IndexOutOfBoundsException();
        }
        toReturn = element[index];
        element[index] = object;
        return toReturn;
    }

    /**
     * Returns the index of the element in list or -1 if there is no such element
     * @param object object index of which need to return
     * @return index of element in list. If the list don't contains this element returns -1
     */
    @Override
    public int indexOf(Object object) {
        int toReturn = -1;
        for (int i = 0; i < element.length; i++) {
            if (element[i].equals(object)) {
                toReturn = i;
            }
        }
        return toReturn;
    }

    /**
     * Adds the specified element to the end of this list.
     * List set limitation to the maximum number of elements.
     * @param e element to be added to this list
     * @return boolean value if an object added in list successfully
     * @throws ListAlreadyFullException if the list size is maximum
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this list
     */
    @Override
    public boolean add(E e) {
        int sizeExpected = size + 1;
        if ((sizeExpected) > MAX_SIZE) {
           throw new ListAlreadyFullException();
        }
        if (sizeExpected > element.length) {
            increase(sizeExpected);
        }
        element[size] = e;
        size++;
        return true;
    }

    /**
     * Increases the capacity for  holding at least the
     * number of elements specified by the minimum capacity argument.
     * @param sizeExpected the desired minimum capacity
     */
    private void increase(int sizeExpected) {
        modCount++;
        Object[] newElement;
       if (sizeExpected < MAX_SIZE) {
           newElement = new Object[sizeExpected];
       } else {
           newElement = new Object[MAX_SIZE];
       }
       int ind = 0;
       for(Object o : element) {
           newElement[ind] = o;
           ind++;
       }
           element = newElement;
       }

    /**
     * Removes the element at the specified position and an empty block
     * @param index the index of the element to remove
     * @return the element that was removed
     * @throws IndexOutOfBoundsException
     */
    public E remove(int index) {
        if ((index > size) | (index < 0)) {
            throw new IndexOutOfBoundsException();
        }
        E toReturn = (E)element[index];
        modCount++;
        int j = 0;
        Object[] after = new Object[size - 1];
        for (int i = 0; i < size; i++) {
            if (i == index) {
                continue;
            }
            after[j] = element[i];
            j++;
        }
        element = after;
        size--;
        return toReturn;
    }
}
