package mog.epam.java_course.home_work08.task1;

public class ListAlreadyFullException extends IllegalArgumentException {
    public ListAlreadyFullException() {
        System.out.println("List already full!");
    }
}
