package mog.epam.java_course.home_work08.task2;

import java.util.*;

public class TenderLogic {
    static int i = 0;
    static boolean complete = false;
    static List<Map<Specialities, Integer>> result;

    /**
     * Calculates required number of people for building an object based on building speciality map
     * @param requiredSources map contains a building speciality and number ofr specialists of this direction
     * @return total number of people for building an object
     */
    static int calculateRequiredPeopleSource(Map<Specialities, Integer> requiredSources) {
        int req = 0;
        Iterator <Map.Entry<Specialities, Integer>> itr = requiredSources.entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry speciality = itr.next();
            req = req + (Integer)speciality.getValue();
        }
        return req;
    }

    /**
     * Check if the total number of construction team workers is greater than requirement. If the
     * team number of workers is less then required the team deleted from the participants list.
     * If after checking the list is empty, method throws NoSuchTeamException
     * @param teamList list contains teams-participants
     * @param req total number of people required for building
     * @throws NoSuchTeamException if neither team does not meet the conditions
     */
    static void checkTheTotalNumber(List teamList, int req) throws NoSuchTeamException {
        Iterator itr = teamList.iterator();
        while(itr.hasNext()) {
            ConstructionTeam team = (ConstructionTeam)itr.next();
            if(team.getTeamList().size() < req) {
                itr.remove();
            }
        }
        if(teamList.size() == 0) {
            throw new NoSuchTeamException();
        }
    }

    /**
     * Selects from a list of teams the team with the right skills and the lowest cost
     * @param participants list of teams applying for a job
     * @param requiredSources list of required specialities
     * @return the team with the right skills and the lowest cost
     * @throws NoSuchTeamException if neither team does not meet the conditions
     */
    static ConstructionTeam chooseTeam(List<ConstructionTeam> participants, Map<Specialities, Integer> requiredSources) throws NoSuchTeamException {
        ConstructionTeam toReturn = null;
        int price = Integer.MAX_VALUE;
        Iterator itr = participants.iterator();
        while(itr.hasNext()) {
            complete = false;
            boolean toRemove = true;
            i = 0;
            List<Map<Specialities, Integer>> currentTeamSpecialitiesList = buildSpecialitiesCombinationsList((ConstructionTeam)itr.next());
            for(Map<Specialities, Integer> curMap : currentTeamSpecialitiesList) {
                if (checkTheSpecList(curMap, requiredSources) == true) {
                    toRemove = false;
                }
            }
            if (toRemove == true) {
               itr.remove();
            }
        }
        for(ConstructionTeam team : participants) {
            if(team.getBusinessProposal() < price) {
                price = team.getBusinessProposal();
            }
        }
        for(ConstructionTeam team : participants) {
            if(team.getBusinessProposal() == price) {
                toReturn = team;
            }
        }
        if (toReturn == null) {
            throw new NoSuchTeamException();
        }
        return toReturn;
    }

    /**
     * Create a list of all possible combinations of specialities of transferred team. In combinations
     * every builder counted once. The first in list is brigadire so his (and only his) skills can be counted several times
     * @param team construction team for combinations creation
     * @return list of all possible combinations of specialities
     */
    private static List<Map<Specialities, Integer>> buildSpecialitiesCombinationsList(ConstructionTeam team) {
        result = new ArrayList<>();
        List<Builder> list = team.getTeamList();
        Map<Specialities, Integer> map = new HashMap<>();
        iterate(list, map);
        return result;
    }

    /**
     * Recursively calls itself and creates possible combinations of professions
     * of the given team
     * @param list list of team's workers
     * @param map map for possible combinations
     */
    private static void iterate(List<Builder> list,  Map<Specialities, Integer> map) {
        while (complete == false) {

            if (i <= list.size() - 2) {
                Iterator itr = list.get(i).getProfessions().iterator();
                while (itr.hasNext()) {
                    i++;
                    Specialities sp = (Specialities) itr.next();
                    Map<Specialities, Integer> created = new HashMap();
                    created = createCopy(map);
                    if(created.containsKey(sp)) {
                        Integer current = created.get(sp);
                        created.remove(sp);
                        created.put(sp,current + 1);
                    } else {
                        created.put(sp, 1);
                        if (complete == true) {
                            complete = false;
                           // newMethod(list, created);
                            iterate(list, created);
                            i--;
                        }
                    }
                    iterate(list, created);
                }

            } else {
                Iterator itr = list.get(i).getProfessions().iterator();
                while (itr.hasNext()) {
                    Specialities sp = (Specialities) itr.next();
                    Map<Specialities, Integer> newcreated = new HashMap();
                    newcreated = createCopy(map);
                    if(newcreated.containsKey(sp)) {
                        Integer current = newcreated.get(sp);
                        newcreated.remove(sp);
                        newcreated.put(sp,current + 1);
                    } else {
                        newcreated.put(sp, 1);
                    }
                    result.add(newcreated);
                }
                complete = true;
                i--;
            }
        }
    }

    /**
     * Create a copy of the transferred map
     * @param sourceMap map need to copy
     * @return copy of transferred map
     */
    private static Map createCopy(Map<Specialities, Integer> sourceMap) {
        Map<Specialities, Integer> result = new HashMap<>();
        Iterator <Map.Entry<Specialities, Integer>> itr = sourceMap.entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry<Specialities, Integer> pair = (Map.Entry<Specialities, Integer>)itr.next();
            Specialities spec = pair.getKey();
            Integer integ = pair.getValue();
            result.put(spec, integ);
        }
        return result;
    }

    /**
     * Checks if transferred map of specialities consists all required specialities in the appropriate amount
     * @param actual map of construction team's specialities
     * @param expected map of required specialities
     * @return boolean value if the specialities appropriate
     */
    private static boolean checkTheSpecList(Map<Specialities, Integer> actual, Map<Specialities, Integer> expected) {
        boolean present = true;
        Iterator <Map.Entry<Specialities, Integer>> itr = expected.entrySet().iterator();
        while(itr.hasNext()) {
            Map.Entry<Specialities, Integer> expectedPair = (Map.Entry<Specialities, Integer>)itr.next();
            Specialities spec = expectedPair.getKey();
            Integer numOfSpec = expectedPair.getValue();
            if(!(actual.containsKey(spec))) {
                present = false;
                break;
            } else if (!(actual.get(spec).equals(numOfSpec))) {
                present = false;
                break;
            }
        }
        return present;
    }
}

