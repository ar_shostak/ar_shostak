package mog.epam.java_course.home_work08.task2;

import java.util.List;
import java.util.Map;

public class Tender {
    private Map<Specialities, Integer> requiredSources;
    private List<ConstructionTeam> participants;

    /**
     * The constructor for tender creation. It contains a map which indicates the minimum number of employees
     * and their specialties, and a list of teams participating in the tender
     * @param requiredSources the required number of workers and their composition
     * @param participants the list of teams participating in tender
     */
    public Tender(Map<Specialities, Integer> requiredSources, List<ConstructionTeam> participants) {
        this.requiredSources = requiredSources;
        this.participants = participants;
    }

    /**
     * Spends a tender and returns the winner
     * @return the winner of the tender
     * @throws NoSuchTeamException it throws if neither team does not meet the conditions of the tender
     */
    public ConstructionTeam conduct() throws NoSuchTeamException {
        int requiredPeopleNumber = TenderLogic.calculateRequiredPeopleSource(requiredSources);
        TenderLogic.checkTheTotalNumber(participants, requiredPeopleNumber);
        ConstructionTeam bestTeam = TenderLogic.chooseTeam(participants, requiredSources);
        return bestTeam;
    }

}
