package mog.epam.java_course.home_work08.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class to represent the builder. Each builder is characterized by a name and a list of professions
 * in which he can work
 */
public class Builder {
    private String name;
    private List<Specialities> professions;

    /**
     * A simple constructor for Builder creation. It uses ArrayList default
     */
    public Builder() {
        this.professions = new ArrayList<Specialities>();
    }

    /**
     * The list  of professions contains Enums for represintation of building professions
     * @param spec this constructor works with the variable number of parameters for easier object creation
     * @param name builder's name
     */
    public Builder(String name, Specialities...spec) {
        this.name = name;
        professions = new ArrayList();
        for(Specialities speciality : spec) {
            professions.add(speciality);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Specialities> getProfessions() {
        return professions;
    }

    public void setProfessions(List<Specialities> professions) {
        this.professions = professions;
    }

    /**
     * Returns the String representation of builder's professionList. Help method for toString
     * @return a String representation of builder list
     */
    private String professionListToString() {
        String str = "";
        for(Specialities prof : professions) {
            str = str + prof + " ";
        }
        return str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Builder builder = (Builder) o;
        return name.equals(builder.name) &&
                professions.equals(builder.professions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, professions);
    }

    @Override
    public String toString() {
        return "Builder{" +
                "name='" + name + '\'' +
                ", professions:"
                + professionListToString() +
                '}';
    }
}
