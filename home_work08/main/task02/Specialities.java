package mog.epam.java_course.home_work08.task2;

/**
 * Enumeration that contains building specialities
 */
public enum Specialities {
    BRICKIE,
    CARPENTER,
    PLASTERER,
    PAINTER,
    CONCRETER
}
