package mog.epam.java_course.home_work08.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class to represent the construction team. It consists a list of all team members.
 */
public class ConstructionTeam {
    String name;
    int businessProposal;
    List<Builder> teamList;

    /**
     * A simple constructor for Builder creation. It uses ArrayList default
     */
    public ConstructionTeam(){
        teamList = new ArrayList<>();
    }
    /**
     * A constructor with initialization of all class fields
     * @param builder the variable number of builders for easier object creation
     * @param name team's name
     * @param businessProposal the price for which the team is ready to do the work
     */
    public ConstructionTeam(String name, int businessProposal, Builder...builder) {
        this.name = name;
        this.businessProposal = businessProposal;
        teamList = new ArrayList<>();
        for(Builder build : builder) {
            teamList.add(build);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBusinessProposal() {
        return businessProposal;
    }

    public void setBusinessProposal(int businessProposal) {
        this.businessProposal = businessProposal;
    }

    public List<Builder> getTeamList() {
        return teamList;
    }

    public void setTeamList(List<Builder> teamList) {
        this.teamList = teamList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConstructionTeam that = (ConstructionTeam) o;
        return businessProposal == that.businessProposal &&
                Objects.equals(name, that.name) &&
                Objects.equals(teamList, that.teamList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, businessProposal, teamList);
    }

    @Override
    public String toString() {
        return "ConstructionTeam{" +
                "name='" + name + '\'' +
                ", businessProposal=" + businessProposal +
                ", teamList=" + teamList +
                '}';
    }
}
