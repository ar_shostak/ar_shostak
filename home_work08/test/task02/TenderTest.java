package mog.epam.java_course.home_work08.task2;

import org.junit.Test;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;

public class TenderTest {

    @Test
    public void testConductTender() throws NoSuchTeamException {
        Builder builder = new Builder("Archy", Specialities.BRICKIE, Specialities.PAINTER);
        Builder builder2 = new Builder("Archy's Brother", Specialities.CARPENTER, Specialities.PLASTERER);
        Builder builder3 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.BRICKIE);
        ConstructionTeam newTeam = new ConstructionTeam("Archi's Comp", 700, builder, builder2, builder3);

        Builder builder7 = new Builder("Profi1", Specialities.BRICKIE);
        Builder builder8 = new Builder("Profi2", Specialities.CARPENTER);
        Builder builder9 = new Builder("Profi3", Specialities.CONCRETER);
        ConstructionTeam newTeam2 = new ConstructionTeam("ProfiBuild.Comp", 500, builder7, builder8, builder9);

        Builder builder4 = new Builder("Vasya", Specialities.CARPENTER, Specialities.PLASTERER, Specialities.PAINTER);
        Builder builder5 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.PAINTER);
        ConstructionTeam Team1 = new ConstructionTeam("Concurents", 600, builder4, builder5);

        Map<Specialities, Integer> requiredSources = new HashMap();
        requiredSources.put(Specialities.BRICKIE, 2);
        requiredSources.put(Specialities.CARPENTER, 1);
        List<ConstructionTeam> participants = new ArrayList<>();
        participants.add(newTeam);
        participants.add(Team1);
        participants.add(newTeam2);
        Tender tender = new Tender(requiredSources, participants);
        ConstructionTeam actual = tender.conduct();
        ConstructionTeam expected = newTeam;
        assertEquals(actual, expected);
    }

    @Test
    public void testConductTenderOtherWinner() throws NoSuchTeamException {
        Builder builder = new Builder("Archy", Specialities.BRICKIE, Specialities.PAINTER);
        Builder builder2 = new Builder("Archy's Brother", Specialities.CARPENTER, Specialities.PLASTERER);
        Builder builder3 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.BRICKIE);
        ConstructionTeam newTeam = new ConstructionTeam("Archi's Comp", 700, builder, builder2, builder3);

        Builder builder7 = new Builder("Profi1", Specialities.BRICKIE);
        Builder builder8 = new Builder("Profi2", Specialities.CARPENTER);
        Builder builder9 = new Builder("Profi3", Specialities.CONCRETER);
        ConstructionTeam newTeam2 = new ConstructionTeam("ProfiBuild.Comp", 500, builder7, builder8, builder9);

        Builder builder4 = new Builder("Vasya", Specialities.CARPENTER, Specialities.PLASTERER, Specialities.PAINTER);
        Builder builder5 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.PAINTER);
        ConstructionTeam Team1 = new ConstructionTeam("Concurents", 600, builder4, builder5);

        Map<Specialities, Integer> requiredSources = new HashMap();
        requiredSources.put(Specialities.BRICKIE, 1);
        requiredSources.put(Specialities.CARPENTER, 1);
        List<ConstructionTeam> participants = new ArrayList<>();
        participants.add(newTeam);
        participants.add(Team1);
        participants.add(newTeam2);
        Tender tender = new Tender(requiredSources, participants);
        ConstructionTeam actual = tender.conduct();
        ConstructionTeam expected = newTeam2;
        assertEquals(actual, expected);
    }

    @Test(expected = NoSuchTeamException.class)
    public void testConductTenderNegativeCase() throws NoSuchTeamException {
        Builder builder = new Builder("Archy", Specialities.BRICKIE, Specialities.PAINTER);
        Builder builder2 = new Builder("Archy's Brother", Specialities.CARPENTER, Specialities.PLASTERER);
        Builder builder3 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.BRICKIE);
        ConstructionTeam newTeam = new ConstructionTeam("Archi's Comp", 700, builder, builder2, builder3);

        Builder builder7 = new Builder("Profi1", Specialities.BRICKIE);
        Builder builder8 = new Builder("Profi2", Specialities.CARPENTER);
        Builder builder9 = new Builder("Profi3", Specialities.CONCRETER);
        ConstructionTeam newTeam2 = new ConstructionTeam("ProfiBuild.Comp", 500, builder7, builder8, builder9);

        Builder builder4 = new Builder("Vasya", Specialities.CARPENTER, Specialities.PLASTERER, Specialities.PAINTER);
        Builder builder5 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.PAINTER);
        ConstructionTeam Team1 = new ConstructionTeam("Concurents", 600, builder4, builder5);

        Map<Specialities, Integer> requiredSources = new HashMap();
        requiredSources.put(Specialities.BRICKIE, 1);
        requiredSources.put(Specialities.CARPENTER, 2);
        List<ConstructionTeam> participants = new ArrayList<>();
        participants.add(newTeam);
        participants.add(Team1);
        participants.add(newTeam2);
        Tender tender = new Tender(requiredSources, participants);
        ConstructionTeam actual = tender.conduct();
    }

    @Test(expected = NoSuchTeamException.class)
    public void testConductTenderNotEnoughtPeople() throws NoSuchTeamException {
        Builder builder = new Builder("Archy", Specialities.BRICKIE, Specialities.PAINTER);
        Builder builder2 = new Builder("Archy's Brother", Specialities.CARPENTER, Specialities.PLASTERER);
        Builder builder3 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.BRICKIE);
        ConstructionTeam newTeam = new ConstructionTeam("Archi's Comp", 700, builder, builder2, builder3);

        Builder builder7 = new Builder("Profi1", Specialities.BRICKIE);
        Builder builder8 = new Builder("Profi2", Specialities.CARPENTER);
        Builder builder9 = new Builder("Profi3", Specialities.CONCRETER);
        ConstructionTeam newTeam2 = new ConstructionTeam("ProfiBuild.Comp", 500, builder7, builder8, builder9);

        Builder builder4 = new Builder("Vasya", Specialities.CARPENTER, Specialities.PLASTERER, Specialities.PAINTER);
        Builder builder5 = new Builder("Archy's 2Brother", Specialities.CONCRETER, Specialities.PAINTER);
        ConstructionTeam Team1 = new ConstructionTeam("Concurents", 600, builder4, builder5);

        Map<Specialities, Integer> requiredSources = new HashMap();
        requiredSources.put(Specialities.CARPENTER, 7);
        List<ConstructionTeam> participants = new ArrayList<>();
        participants.add(newTeam);
        participants.add(Team1);
        participants.add(newTeam2);
        Tender tender = new Tender(requiredSources, participants);
        ConstructionTeam actual = tender.conduct();
    }

}
