package mog.epam.java_course.home_work08.task1;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;

public class MyListTest {
    List fullList;
    List testList;

    @Before
    public void createTestObject() {
        fullList = new MyList(20);
        for (int i = 0; i < 20; i++) {
            fullList.add("Java");
        }
        testList = new MyList(3);
        testList.add(33);
        testList.add(66);
        testList.add(99);
    }

    @After
    public void clearTestObject() {
        fullList = null;
        testList = null;
    }

    @Test
    public void testAdd() {
        List actual = new MyList(5);
        actual.add(2);
        actual.add(4);
        actual.add(37);
        List expected = new ArrayList();
        expected.add(2);
        expected.add(4);
        expected.add(37);
        assertEquals(expected, actual);
    }

    @Test(expected = ListAlreadyFullException.class)
    public void testAddNegative() {
        List actual = fullList;
        actual.add("One more Java");
    }

    @Test
    public void testGet() {
        Object actual = fullList.get(2);
        Object expected = "Java";
        assertEquals(expected, actual);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testGetNegative() {
        testList.get(10);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testGetNegativeIndex() {
        testList.get(-1);
    }

    @Test
    public void testSize() {
        int expected = fullList.size();
        assertEquals(expected, 20);
    }

    @Test
    public void testIndexOf() {
        List someList = new MyList(3);
        someList.add(2);
        someList.add(4);
        someList.add(37);
        int expected = 1;
        assertEquals(expected, someList.indexOf(4));
    }

    @Test
    public void testIndexOfNegative() {
        List someList = new MyList(5);
        someList.add("Hello");
        someList.add("Jt");
        someList.add("Mog");
        int actual = someList.indexOf("Minsk");
        assertEquals(actual, -1);
    }

    @Test
    public void testRemove() {
        testList.remove(1);
        List actual = testList;
        List expected = new ArrayList();
        expected.add(33);
        expected.add(99);
        assertEquals(actual, expected);
    }

    @Test (expected = IndexOutOfBoundsException.class)
    public void testRemoveNegative() {
        testList.remove(15);
    }

    @Test
    public void testSet() {
        testList.set(1, 1);
        List expected = new ArrayList();
        expected.add(33);
        expected.add(1);
        expected.add(99);
        assertEquals(expected, testList);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testSetNegative() {
        testList.set(15, 1);
    }
}
